﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts
{
    public interface IRepositoryCustomers : IRepository<Customer>
    {
        Customers Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDcustomer", string searchText = "");

        Customer Get(string userName);
    }
}
