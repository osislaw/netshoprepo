﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts
{
    public interface IRepositoryPaymentTypes : IRepository<PaymentType>
    {
        PaymentTypes Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDpayment", string searchText = "");
    }
}
