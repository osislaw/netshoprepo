﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts
{
    public interface IRepositoryContactShop : IRepository<ContactShop>
    {
        ContactShops Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDcontactShop", string searchText = "");
    }
}
