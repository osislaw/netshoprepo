﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts
{
    public interface IRepositoryUserProfiles : IRepository<UserProfile>
    {
        UserProfiles Search(int jtStartIndex, int jtPageSize, string jtSorting = "UserId", string searchText = "");
        bool UpdateUserName(string newUserName, int userId);

        List<UserProfile> GetUsers(int jtStartIndex, int jtPageSize, string jtSorting = "UserName", string searchText = null);
    }
}
