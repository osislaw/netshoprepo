﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts
{
    public interface IRepositoryProducts : IRepository<Product>
    {
        Products Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDproduct", string searchText = "");

        Images GetProductImages(int productID);

        int AddImages(Image image);

        Image GetImage(int imageID);

        bool DeleteImage(int imageID);

        bool UpdateImages(Image image);

        Image GetMainImage(int productID);

        Products GetCategoryProducts(int categoryID, int pageSize, int pageNumber, string orderBy);
    }
}
