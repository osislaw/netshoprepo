﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts
{
    public interface IRepositoryPaymentShipment : IRepository<PaymentShipment>
    {
        PaymentsShipments Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDpaymentShipment", string searchText = "");
        bool PaymentShipmentExists(int idPayment, int idShipment);
    }
}
