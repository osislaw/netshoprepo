﻿using NetShop.Contracts.Class;
using System.Collections.Generic;

namespace NetShop.Contracts
{
    public interface IRepositoryRealizations : IRepository<Realization>
    {
        Realizations Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDrealization", string searchText = "");

        bool AddToBasket(Basket basket);

        void RemoveItemsFromBasket(string sesssionID);

        List<Basket> GetBasket(string sessionID);

        bool RemoveItemFromBasket(string sesssionID, int productID);

        decimal UpdateProductCount(string sessionID, int productID, int productCount);

        int AddAddress(RealizationAddress address);

        void AddItemsToRealization(string sessionID, int realizationID);

        int AddRealization(Realization realization, RealizationAddress address, string sessionID);

        RealizationResult GetRealizationResult(int realizationID);

        Realizations GetByCustomer(int customerID, int pageNumber, int pageSize);

        List<Order> GetItems(int realizationID);

        RealizationItems GetRealizationItems(int realizationID);

        List<RealizationStatus> GetStatusList();

        RealizationItem AddItemToRealization(RealizationItem realizationItem);

        RealizationItem UpdateItemInRealization(RealizationItem realizationItem);

        bool DeleteItemFromRealization(int realizationItemID);
    }
}
