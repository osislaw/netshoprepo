﻿using NetShop.Contracts.Class;

namespace NetShop.Contracts
{
    public interface IRepositoryValueTypes : IRepository<Class.ValueType>
    {
        ValueTypes Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDvalueType", string searchText = "");
    }
}
