﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts
{
    public interface IRepositoryShipmentTypes : IRepository<ShipmentType>
    {
        ShipmentTypes Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDshipment", string searchText = "");

        ShipmentTypes SearchShipmentByPayment(int paymentID);
    }
}
