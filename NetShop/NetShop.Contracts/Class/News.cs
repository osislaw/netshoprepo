﻿using System.Collections.Generic;

namespace NetShop.Contracts.Class
{
    public class News
    {
        private List<NewsItem> newsItemList;
        private int totalCount;

        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; }
        }

        public List<NewsItem> NewsItemList
        {
            get { return newsItemList; }
            set { newsItemList = value; }
        }
    }
}
