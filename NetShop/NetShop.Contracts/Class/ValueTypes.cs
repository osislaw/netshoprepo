﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class ValueTypes
    {
        private List<ValueType> valueTypeList;
        private int totalCount;

        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; }
        }

        public List<ValueType> ValueTypeList
        {
            get { return valueTypeList; }
            set { valueTypeList = value; }
        }
    }
}
