﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class Image
    {
        private int iDimage;
        private string path;
        private int productID;
        private bool isActive;
        private bool isMain;

        public bool IsMain
        {
            get { return isMain; }
            set { isMain = value; }
        }

        public bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public int ProductID
        {
            get { return productID; }
            set { productID = value; }
        }

        public string Path
        {
            get { return path; }
            set { path = value; }
        }
        
        public int IDimage
        {
            get { return iDimage; }
            set { iDimage = value; }
        }
    }
}
