﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public enum ItemStatus
    {
        LackOfProduct = 1,
        IsOK = 2,
        Canceled = 3,
        Returned = 4
    }
}
