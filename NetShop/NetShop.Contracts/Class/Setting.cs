﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class Setting
    {
        private int iDsetting;
        private string code;
        private string value;
        private string name;
        private string description;
        private int valueTypeID;
        private string valueName;

        public string ValueName
        {
            get { return valueName; }
            set { valueName = value; }
        }
        private bool isDeleted;
        private string creator;

        public int IDsetting
        {
            get
            {
                return iDsetting;
            }

            set
            {
                iDsetting = value;
            }
        }

        public string Code
        {
            get
            {
                return code;
            }

            set
            {
                code = value;
            }
        }

        public string Value
        {
            get
            {
                return value;
            }

            set
            {
                this.value = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
            }
        }

        public bool IsDeleted
        {
            get
            {
                return isDeleted;
            }

            set
            {
                isDeleted = value;
            }
        }

        public int ValueTypeID
        {
            get
            {
                return valueTypeID;
            }

            set
            {
                valueTypeID = value;
            }
        }

        public string Creator
        {
            get
            {
                return creator;
            }

            set
            {
                creator = value;
            }
        }
    }
}
