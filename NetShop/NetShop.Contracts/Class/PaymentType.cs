﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class PaymentType
    {
        private int iDpayment;
        private string paymentName;
        private bool isActive;
        private string code;

        public int IDpayment
        {
            get
            {
                return iDpayment;
            }

            set
            {
                iDpayment = value;
            }
        }

        public string PaymentName
        {
            get
            {
                return paymentName;
            }

            set
            {
                paymentName = value;
            }
        }

        public bool IsActive
        {
            get
            {
                return isActive;
            }

            set
            {
                isActive = value;
            }
        }

        public string Code
        {
            get
            {
                return code;
            }

            set
            {
                code = value;
            }
        }
    }
}
