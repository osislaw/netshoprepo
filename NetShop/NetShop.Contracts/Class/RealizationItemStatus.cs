﻿
namespace NetShop.Contracts.Class
{
    public class RealizationItemStatus
    {
        private int iDrealizationItemStatus;
        private string statusName;

        public string StatusName
        {
            get { return statusName; }
            set { statusName = value; }
        }

        public int IDrealizationItemStatus
        {
            get { return iDrealizationItemStatus; }
            set { iDrealizationItemStatus = value; }
        }
    }
}
