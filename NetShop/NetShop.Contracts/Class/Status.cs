﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public enum Status
    {
        New = 1,
        ToPay = 2,
        ToSend = 3,
        Send = 4,
        Finished = 5
    }
}
