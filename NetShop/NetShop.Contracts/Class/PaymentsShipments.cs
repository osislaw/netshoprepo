﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class PaymentsShipments
    {
        private int totalCount;
        private List<PaymentShipment> paymentShipmentList;

        public List<PaymentShipment> PaymentShipmentList
        {
            get { return paymentShipmentList; }
            set { paymentShipmentList = value; }
        }

        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; }
        }
    }
}
