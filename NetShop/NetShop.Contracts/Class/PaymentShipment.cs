﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class PaymentShipment
    {
        private int iDpaymentShipment;
        private int paymentID;
        private string paymentName;
        private int shipmentID;
        private string shipmentName;

        public int IDpaymentShipment
        {
            get
            {
                return iDpaymentShipment;
            }

            set
            {
                iDpaymentShipment = value;
            }
        }

        public int PaymentID
        {
            get
            {
                return paymentID;
            }

            set
            {
                paymentID = value;
            }
        }

        public string PaymentName
        {
            get
            {
                return paymentName;
            }

            set
            {
                paymentName = value;
            }
        }

        public int ShipmentID
        {
            get
            {
                return shipmentID;
            }

            set
            {
                shipmentID = value;
            }
        }

        public string ShipmentName
        {
            get
            {
                return shipmentName;
            }

            set
            {
                shipmentName = value;
            }
        }
    }
}
