﻿
namespace NetShop.Contracts.Class
{
    public class ContactShop
    {
        private int iDcontactShop;
        private string phone;
        private string city;
        private string street;
        private string zipCode;
        private string nip;
        private string ownerFirstName;
        private string ownerSurname;

        public string OwnerSurname
        {
            get { return ownerSurname; }
            set { ownerSurname = value; }
        }

        public string OwnerFirstName
        {
            get { return ownerFirstName; }
            set { ownerFirstName = value; }
        }

        public string NIP
        {
            get { return nip; }
            set { nip = value; }
        }

        public string ZipCode
        {
            get { return zipCode; }
            set { zipCode = value; }
        }

        public string Street
        {
            get { return street; }
            set { street = value; }
        }

        public string City
        {
            get { return city; }
            set { city = value; }
        }

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

        public int IDcontactShop
        {
            get { return iDcontactShop; }
            set { iDcontactShop = value; }
        }
    }
}
