﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class UserProfiles
    {
        private List<UserProfile> userProfileList;
        private int totalCount;

        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; }
        }

        public List<UserProfile> UserProfileList
        {
            get { return UserProfileList; }
            set { UserProfileList = value; }
        }
    }
}
