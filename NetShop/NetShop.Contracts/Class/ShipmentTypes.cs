﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class ShipmentTypes
    {
        private int totalCount;
        private List<ShipmentType> shipmentTypeList;

        public List<ShipmentType> ShipmentTypeList
        {
            get
            {
                return shipmentTypeList;
            }

            set
            {
                shipmentTypeList = value;
            }
        }

        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; }
        }

    }
}
