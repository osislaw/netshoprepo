﻿
namespace NetShop.Contracts.Class
{
    public class RealizationItem
    {
        private int iDrealizationItem;
        private int realizationID;
        private int productID;
        private decimal productPrice;
        private decimal productPriceBrutto;
        private string productName;
        private int realizationItemStatusID;
        private int productCount;

        public int ProductCount
        {
            get { return productCount; }
            set { productCount = value; }
        }

        public int RealizationItemStatusID
        {
            get { return realizationItemStatusID; }
            set { realizationItemStatusID = value; }
        }

        public string ProductName
        {
            get { return productName; }
            set { productName = value; }
        }

        public decimal ProductPriceBrutto
        {
            get { return productPriceBrutto; }
            set { productPriceBrutto = value; }
        }

        public decimal ProductPrice
        {
            get { return productPrice; }
            set { productPrice = value; }
        }

        public int ProductID
        {
            get { return productID; }
            set { productID = value; }
        }

        public int RealizationID
        {
            get { return realizationID; }
            set { realizationID = value; }
        }

        public int IDrealizationItem
        {
            get { return iDrealizationItem; }
            set { iDrealizationItem = value; }
        }
    }
}
