﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class Images
    {
        private int totalCount;
        private List<Image> imageList;

        public List<Image> ImageList
        {
            get { return imageList; }
            set { imageList = value; }
        }

        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; }
        }
    }
}
