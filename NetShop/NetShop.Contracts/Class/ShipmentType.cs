﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class ShipmentType
    {
        private int iDshipment;
        private string shipmentName;
        private decimal shipmentPrice;
        private bool isActive;
        private string code;
        private string shipmentValue;

        public string ShipmentValue
        {
            get { return ShipmentName + " " + ShipmentPrice /*+ "WALUTA"*/; } // TODO USTAWIENIE GLOBALNE WALUTY
        }

        public int IDshipment
        {
            get
            {
                return iDshipment;
            }

            set
            {
                iDshipment = value;
            }
        }

        public string ShipmentName
        {
            get
            {
                return shipmentName;
            }

            set
            {
                shipmentName = value;
            }
        }

        public decimal ShipmentPrice
        {
            get
            {
                return shipmentPrice;
            }

            set
            {
                shipmentPrice = value;
            }
        }

        public bool IsActive
        {
            get
            {
                return isActive;
            }

            set
            {
                isActive = value;
            }
        }

        public string Code
        {
            get
            {
                return code;
            }

            set
            {
                code = value;
            }
        }
    }
}
