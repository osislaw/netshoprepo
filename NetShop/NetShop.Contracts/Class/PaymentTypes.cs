﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class PaymentTypes
    {
        private int totalCount;
        private List<PaymentType> paymentTypeList;

        public List<PaymentType> PaymentTypeList
        {
            get { return paymentTypeList; }
            set { paymentTypeList = value; }
        }

        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; }
        }
    }
}
