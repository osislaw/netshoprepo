﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class Basket
    {
        private int iDbasket;
        private string sessionID;
        private int productID;
        private int productCount;
        private string productName;
        private Image productImage;
        private decimal priceBrutto;
        private string unit;

        public string Unit
        {
            get { return unit; }
            set { unit = value; }
        }

        public decimal PriceBrutto
        {
            get { return priceBrutto; }
            set { priceBrutto = value; }
        }

        public string ProductName
        {
            get { return productName; }
            set { productName = value; }
        }

        public Image ProductImage
        {
            get { return productImage; }
            set { productImage = value; }
        }

        public int ProductCount
        {
            get { return productCount; }
            set { productCount = value; }
        }

        public int ProductID
        {
            get { return productID; }
            set { productID = value; }
        }

        public string SessionID
        {
            get { return sessionID; }
            set { sessionID = value; }
        }

        public int IDbasket
        {
            get { return iDbasket; }
            set { iDbasket = value; }
        }
    }
}
