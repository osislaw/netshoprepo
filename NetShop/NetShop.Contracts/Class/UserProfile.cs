﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class UserProfile
    {
        private int userId;
        private string userName;
        private string password;

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public int UserId
        {
            get
            {
                return userId;
            }

            set
            {
                userId = value;
            }
        }

        public string UserName
        {
            get
            {
                return userName;
            }

            set
            {
                userName = value;
            }
        }
    }
}
