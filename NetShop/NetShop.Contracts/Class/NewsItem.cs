﻿using System;

namespace NetShop.Contracts.Class
{
    public class NewsItem
    {
        private int iDnews;
        private string title;
        private string description;
        private DateTime createDate;
        private string author;
        private string shortDescription;
        private bool isDeleted;

        public bool IsDeleted
        {
            get { return isDeleted; }
            set { isDeleted = value; }
        }

        public string ShortDescription
        {
            get { return shortDescription; }
            set { shortDescription = value; }
        }

        public string Author
        {
            get { return author; }
            set { author = value; }
        }

        public DateTime CreateDate
        {
            get { return createDate; }
            set { createDate = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        public int IDnews
        {
            get { return iDnews; }
            set { iDnews = value; }
        }
    }
}
