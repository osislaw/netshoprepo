﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class Settings
    {
        private List<Setting> settingsList;
        private int totalCount;

        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; }
        }

        public List<Setting> SettingsList
        {
            get { return settingsList; }
            set { settingsList = value; }
        }
    }
}
