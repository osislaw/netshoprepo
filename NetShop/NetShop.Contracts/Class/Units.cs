﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class Units
    {
        private List<Unit> unitList;
        private int totalCount;

        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; }
        }

        public List<Unit> UnitList
        {
            get { return unitList; }
            set { unitList = value; }
        }
    }
}
