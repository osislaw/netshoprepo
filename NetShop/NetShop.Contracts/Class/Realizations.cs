﻿using System.Collections.Generic;

namespace NetShop.Contracts.Class
{
    public class Realizations
    {
        private List<Realization> realizationList;
        private int totalCount;

        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; }
        }

        public List<Realization> RealizationList
        {
            get { return realizationList; }
            set { realizationList = value; }
        }
    }
}
