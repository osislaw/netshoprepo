﻿using System.Collections.Generic;

namespace NetShop.Contracts.Class
{
    public class Categories
    {
        private List<Category> categoryList;
        private int totalCount;

        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; }
        }

        public List<Category> CategoryList
        {
            get { return categoryList; }
            set { categoryList = value; }
        }
    }
}
