﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class Order
    {
        private int iDrealizationItem;
        private int realizationID;
        private int productID;
        private decimal productPrice;
        private decimal productPriceBrutto;
        private string productName;
        private int realizationItemStatusID;
        private int orderedCount;
        private Image productImage;
        private string realizationItemStatus;
        private string unit;

        public string Unit
        {
            get { return unit; }
            set { unit = value; }
        }

        public string RealizationItemStatus
        {
            get { return realizationItemStatus; }
            set { realizationItemStatus = value; }
        }

        public Image ProductImage
        {
            get { return productImage; }
            set { productImage = value; }
        }

        public int OrderedCount
        {
            get { return orderedCount; }
            set { orderedCount = value; }
        }

        public int RealizationItemStatusID
        {
            get { return realizationItemStatusID; }
            set { realizationItemStatusID = value; }
        }

        public string ProductName
        {
            get { return productName; }
            set { productName = value; }
        }

        public decimal ProductPriceBrutto
        {
            get { return productPriceBrutto; }
            set { productPriceBrutto = value; }
        }

        public decimal ProductPrice
        {
            get { return productPrice; }
            set { productPrice = value; }
        }

        public int ProductID
        {
            get { return productID; }
            set { productID = value; }
        }

        public int RealizationID
        {
            get { return realizationID; }
            set { realizationID = value; }
        }

        public int IDrealizationItem
        {
            get { return iDrealizationItem; }
            set { iDrealizationItem = value; }
        }
    }
}
