﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class RealizationAddress
    {
        private int iDrealizationAddress;
        private string firstName;
        private string surname;
        private string city;
        private string street;
        private string zipCode;
        private string companyNIP;
        private string companyStreet;
        private string companyCity;
        private string companyZipCode;
        private string companyName;
        private string email;
        private string phone;

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string CompanyName
        {
            get { return companyName; }
            set { companyName = value; }
        }

        public string CompanyZipCode
        {
            get { return companyZipCode; }
            set { companyZipCode = value; }
        }

        public string CompanyCity
        {
            get { return companyCity; }
            set { companyCity = value; }
        }

        public string CompanyStreet
        {
            get { return companyStreet; }
            set { companyStreet = value; }
        }

        public string CompanyNIP
        {
            get { return companyNIP; }
            set { companyNIP = value; }
        }

        public string ZipCode
        {
            get { return zipCode; }
            set { zipCode = value; }
        }

        public string Street
        {
            get { return street; }
            set { street = value; }
        }

        public string City
        {
            get { return city; }
            set { city = value; }
        }

        public string Surname
        {
            get { return surname; }
            set { surname = value; }
        }

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        public int IDrealizationAddress
        {
            get { return iDrealizationAddress; }
            set { iDrealizationAddress = value; }
        }
    }
}
