﻿namespace NetShop.Contracts.Class
{
    public class Customer
    {
        private int iDcustomer;
        private string firstName;
        private string surname;
        private string city;
        private string street;
        private string zipCode;
        private string companyNIP;
        private string companyCity;
        private string companyStreet;
        private string companyZipCode;
        private int userProfileID;
        private string email;
        private string phone;
        private bool isDeleted;
        private string companyName;

        //login/haslo do logowania
        private string userName;
        private string password;
        private string confirmPassword;

        public string CompanyName
        {
            get { return companyName; }
            set { companyName = value; }
        }

        public bool IsDeleted
        {
            get { return isDeleted; }
            set { isDeleted = value; }
        }

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string CompanyZipCode
        {
            get { return companyZipCode; }
            set { companyZipCode = value; }
        }

        public string CompanyStreet
        {
            get { return companyStreet; }
            set { companyStreet = value; }
        }

        public string CompanyCity
        {
            get { return companyCity; }
            set { companyCity = value; }
        }

        public string CompanyNIP
        {
            get { return companyNIP; }
            set { companyNIP = value; }
        }

        public string ZipCode
        {
            get { return zipCode; }
            set { zipCode = value; }
        }

        public string Street
        {
            get { return street; }
            set { street = value; }
        }

        public string City
        {
            get { return city; }
            set { city = value; }
        }

        public string Surname
        {
            get { return surname; }
            set { surname = value; }
        }

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        public int IDcustomer
        {
            get { return iDcustomer; }
            set { iDcustomer = value; }
        }

        public int UserProfileID
        {
            get
            {
                return userProfileID;
            }

            set
            {
                userProfileID = value;
            }
        }

        public string UserName
        {
            get
            {
                return userName;
            }

            set
            {
                userName = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        public string ConfirmPassword
        {
            get
            {
                return confirmPassword;
            }

            set
            {
                confirmPassword = value;
            }
        }
    }
}
