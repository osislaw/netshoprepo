﻿using System.Collections.Generic;

namespace NetShop.Contracts.Class
{
    public class RealizationsAddress
    {
        private List<RealizationAddress> realizationAddressList;
        private int totalCount;

        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; }
        }

        public List<RealizationAddress> RealizationAddressList
        {
            get { return realizationAddressList; }
            set { realizationAddressList = value; }
        }
    }
}
