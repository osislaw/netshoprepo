﻿using System.Collections.Generic;

namespace NetShop.Contracts.Class
{
    public class ContactShops
    {
        private List<ContactShop> contactShopList;
        private int totalCount;

        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; }
        }

        public List<ContactShop> ContactShopList
        {
            get { return contactShopList; }
            set { contactShopList = value; }
        }
    }
}
