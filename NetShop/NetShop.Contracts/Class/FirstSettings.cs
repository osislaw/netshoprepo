﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class FirstSettings
    {
        private bool firstRun;
        private int newsCount;
        private string currency;
        private string shopName;
        private string contact;
        private string regulations;
        private string cookies;
        private string returnPolicy;

        public bool FirstRun
        {
            get
            {
                return firstRun;
            }

            set
            {
                firstRun = value;
            }
        }

        public int NewsCount
        {
            get
            {
                return newsCount;
            }

            set
            {
                newsCount = value;
            }
        }

        public string Currency
        {
            get
            {
                return currency;
            }

            set
            {
                currency = value;
            }
        }

        public string ShopName
        {
            get
            {
                return shopName;
            }

            set
            {
                shopName = value;
            }
        }

        public string Contact
        {
            get
            {
                return contact;
            }

            set
            {
                contact = value;
            }
        }

        public string Regulations
        {
            get
            {
                return regulations;
            }

            set
            {
                regulations = value;
            }
        }

        public string Cookies
        {
            get
            {
                return cookies;
            }

            set
            {
                cookies = value;
            }
        }

        public string ReturnPolicy
        {
            get
            {
                return returnPolicy;
            }

            set
            {
                returnPolicy = value;
            }
        }
    }
}
