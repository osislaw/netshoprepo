﻿using System.Collections.Generic;

namespace NetShop.Contracts.Class
{
    public class RealizationItems
    {
        private List<RealizationItem> realizationItemList;
        private int totalCount;

        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; }
        }

        public List<RealizationItem> RealizationItemList
        {
            get { return realizationItemList; }
            set { realizationItemList = value; }
        }
    }
}
