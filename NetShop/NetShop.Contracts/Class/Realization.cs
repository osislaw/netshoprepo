﻿using System;

namespace NetShop.Contracts.Class
{
    public class Realization
    {
        private int iDrealization;
        private int customerID;
        private decimal? totalAmount;
        private decimal? totalAmountBrutto;
        private DateTime createDate;
        private DateTime? endDate;
        private bool isDeleted;
        private string comment;
        private int realizationStatusID;
        private int realizationAddressID;
        private int paymentID;
        private int shipmentID;
        private string paymentName;
        private string shipmentName;
        private string statusName;
        private string customerName;

        public string ShipmentName
        {
            get { return shipmentName; }
            set { shipmentName = value; }
        }

        public string StatusName
        {
            get { return statusName; }
            set { statusName = value; }
        }

        public string PaymentName
        {
            get { return paymentName; }
            set { paymentName = value; }
        }

        public int ShipmentID
        {
            get { return shipmentID; }
            set { shipmentID = value; }
        }

        public int PaymentID
        {
            get { return paymentID; }
            set { paymentID = value; }
        }

        public int RealizationAddressID
        {
            get { return realizationAddressID; }
            set { realizationAddressID = value; }
        }

        public int RealizationStatusID
        {
            get { return realizationStatusID; }
            set { realizationStatusID = value; }
        }

        public string Comment
        {
            get { return comment; }
            set { comment = value; }
        }

        public bool IsDeleted
        {
            get { return isDeleted; }
            set { isDeleted = value; }
        }

        public DateTime? EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }

        public DateTime CreateDate
        {
            get { return createDate; }
            set { createDate = value; }
        }

        public decimal? TotalAmountBrutto
        {
            get { return totalAmountBrutto; }
            set { totalAmountBrutto = value; }
        }

        public decimal? TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; }
        }

        public int CustomerID
        {
            get { return customerID; }
            set { customerID = value; }
        }

        public int IDrealization
        {
            get { return iDrealization; }
            set { iDrealization = value; }
        }

        public string CustomerName
        {
            get
            {
                return customerName;
            }

            set
            {
                customerName = value;
            }
        }
    }
}
