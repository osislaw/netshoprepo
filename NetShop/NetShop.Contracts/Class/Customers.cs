﻿using System.Collections.Generic;

namespace NetShop.Contracts.Class
{
    public class Customers
    {
        private List<Customer> customerList;
        private int totalCount;

        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; }
        }

        public List<Customer> CustomerList
        {
            get { return customerList; }
            set { customerList = value; }
        }
    }
}
