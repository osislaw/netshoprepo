﻿
namespace NetShop.Contracts.Class
{
    public class RealizationStatus
    {
        private int realizationStatusID;
        private string statusName;

        public string StatusName
        {
            get { return statusName; }
            set { statusName = value; }
        }

        public int RealizationStatusID
        {
            get { return realizationStatusID; }
            set { realizationStatusID = value; }
        }
    }
}
