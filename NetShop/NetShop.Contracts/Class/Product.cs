﻿using System.Collections.Generic;

namespace NetShop.Contracts.Class
{
    public class Product
    {
        private int iDproduct;
        private string productName;
        private int categoryID;
        private bool visible;
        private bool isDeleted;
        private int weight;
        private decimal priceNetto;
        private decimal priceBrutto;
        private string producerCode;
        private string description;
        private Image productImage;
        private int? unitID;
        private string unit;

        public Image ProductImage
        {
            get { return productImage; }
            set { productImage = value; }
        } 

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public string ProducerCode
        {
            get { return producerCode; }
            set { producerCode = value; }
        }

        public decimal PriceBrutto
        {
            get { return priceBrutto; }
            set { priceBrutto = value; }
        }

        public decimal PriceNetto
        {
            get { return priceNetto; }
            set { priceNetto = value; }
        }

        public int Weight
        {
            get { return weight; }
            set { weight = value; }
        }

        public bool IsDeleted
        {
            get { return isDeleted; }
            set { isDeleted = value; }
        }

        public bool Visible
        {
            get { return visible; }
            set { visible = value; }
        }

        public int CategoryID
        {
            get { return categoryID; }
            set { categoryID = value; }
        }

        public string ProductName
        {
            get { return productName; }
            set { productName = value; }
        }

        public int IDproduct
        {
            get { return iDproduct; }
            set { iDproduct = value; }
        }

        public string Unit
        {
            get
            {
                return unit;
            }

            set
            {
                unit = value;
            }
        }

        public int? UnitID
        {
            get
            {
                return unitID;
            }

            set
            {
                unitID = value;
            }
        }
    }
}
