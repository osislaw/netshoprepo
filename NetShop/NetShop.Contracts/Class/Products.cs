﻿using System.Collections.Generic;

namespace NetShop.Contracts.Class
{
    public class Products
    {
        private List<Product> productList;
        private int totalCount;

        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; }
        }

        public List<Product> ProductList
        {
            get { return productList; }
            set { productList = value; }
        }
    }
}
