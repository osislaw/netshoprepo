﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts.Class
{
    public class TreeCategory
    {
        private List<LeafCategory> leafCategory;
        private string categoryName;
        private int iDCategory;

        public int IDCategory
        {
            get { return iDCategory; }
            set { iDCategory = value; }
        }

        public string CategoryName
        {
            get { return categoryName; }
            set { categoryName = value; }
        }        

        public List<LeafCategory> LeafCategory
        {
            get { return leafCategory; }
            set { leafCategory = value; }
        }
    }
}
