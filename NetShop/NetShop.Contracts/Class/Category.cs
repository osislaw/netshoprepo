﻿
namespace NetShop.Contracts.Class
{
    public class Category
    {
        private int iDcategory;
        private int parentID;
        private string categoryName;
        private string description;
        private bool visible;
        private bool isDeleted;

        public bool IsDeleted
        {
            get { return isDeleted; }
            set { isDeleted = value; }
        }

        public bool Visible
        {
            get { return visible; }
            set { visible = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public string CategoryName
        {
            get { return categoryName; }
            set { categoryName = value; }
        }

        public int ParentID
        {
            get { return parentID; }
            set { parentID = value; }
        }

        public int IDcategory
        {
            get { return iDcategory; }
            set { iDcategory = value; }
        }
    }
}
