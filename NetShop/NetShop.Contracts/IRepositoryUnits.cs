﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts
{
    public interface IRepositoryUnits : IRepository<Unit>
    {
        Units Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDunit", string searchText = "");
    }
}
