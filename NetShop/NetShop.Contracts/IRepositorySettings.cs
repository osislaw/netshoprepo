﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts
{
    public interface IRepositorySettings : IRepository<Setting>
    {
        Settings Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDsetting", string searchText = "");

        Setting GetByCode(string code);
    }
}
