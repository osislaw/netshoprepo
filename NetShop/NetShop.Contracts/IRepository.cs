﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts
{
    public interface IRepository<T>
    {
        int Add(T addItem);

        bool Update(T updateItem);

        bool Delete(int deleteItem);

        T Get(int itemID);
    }
}
