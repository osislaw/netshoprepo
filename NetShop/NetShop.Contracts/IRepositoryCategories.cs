﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts
{
    public interface IRepositoryCategories : IRepository<Category>
    {
        Categories Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDcategory", string searchText = "");

        List<TreeCategory> GetTree();

        List<Category> GetChildrenCategory(int parentID);

        Categories SearchCategories(int startIndex, int pageSize, string sorting = "IDcategory", string searchText = "");
    }
}
