﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Contracts
{
    public interface IRepositoryNews : IRepository<NewsItem>
    {
        News Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDnews", string searchText = "");
    }
}
