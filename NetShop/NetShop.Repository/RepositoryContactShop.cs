﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetShop.Contracts;
using Entities = NetShop.Contracts.Class;

namespace NetShop.Repository
{
    public class RepositoryContactShop : IRepositoryContactShop
    {
        public int Add(Entities.ContactShop contactShop)
        {
            return -1;
        }

        public bool Update(Entities.ContactShop contactShop)
        {
            return false;
        }

        public bool Delete(int contactShopID)
        {
            return false;
        }

        public Entities.ContactShop Get(int contactShopID)
        {
            return new Entities.ContactShop();
        }

        public Entities.ContactShops Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDcontactShop", string searchText = "")
        {
            return new Entities.ContactShops();
        }
    }
}
