﻿using AutoMapper;
using NetShop.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using Entities = NetShop.Contracts.Class;
using System.Linq.Dynamic;

namespace NetShop.Repository
{
    public class RepositoryUserProfiles : IRepositoryUserProfiles
    {
        NetShopEntities context = new NetShopEntities();

        public RepositoryUserProfiles()
        {
            Mapper.CreateMap<Entities.UserProfile, UserProfile>();
            Mapper.CreateMap<UserProfile, Entities.UserProfile>();
        }
        public int Add(Entities.UserProfile userProfile)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int userId)
        {
            throw new NotImplementedException();
        }

        public Entities.UserProfile Get(int userId)
        {
            UserProfile userProfile = new UserProfile();
            try
            {
                userProfile = context.UserProfile.Where(x => x.UserId == userId).FirstOrDefault();
            }
            catch (SqlException ex)
            {
            }
            Entities.UserProfile newUserProfile = Mapper.Map<Entities.UserProfile>(userProfile);
            return newUserProfile;
        }

        public Entities.UserProfiles Search(int jtStartIndex, int jtPageSize, string jtSorting = "UserId", string searchText = "")
        {
            throw new NotImplementedException();
        }

        public bool Update(Entities.UserProfile userProfile)
        {
            UserProfile profile = Mapper.Map<UserProfile>(userProfile);
            if (profile != null)
            {
                try
                {
                    context.Entry(profile).State = EntityState.Modified;
                    context.SaveChanges();
                }
                catch (SqlException ex)
                {
                    return false;
                }
            }
            return true;
        }

        public bool UpdateUserName(string newUserName, int userId)
        {
            try
            {
                UserProfile profile = context.UserProfile.Find(userId);
                profile.UserName = newUserName;
                context.Entry(profile).State = EntityState.Modified;
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public List<Entities.UserProfile> GetUsers(int startIndex, int pageSize, string sorting = "UserName", string searchText = null)
        {
            var users = (from user in context.UserProfile
                         select user);

            string sql = "Select u.UserId, u.UserName, r.RoleId from UserProfile u " +
                              "left join webpages_UsersInRoles ur on ur.UserId=u.UserId " +
                              "left join webpages_Roles r on r.RoleId=ur.RoleId " +
                              "where r.RoleName like 'Admin'";
            if (string.IsNullOrEmpty(searchText))
            {
                users = context.UserProfile.SqlQuery(sql).AsQueryable();
            }
            if (!string.IsNullOrEmpty(searchText))
            {
                sql = "Select u.UserId, u.UserName, r.RoleId from UserProfile u " +
                                  "left join webpages_UsersInRoles ur on ur.UserId=u.UserId " +
                                  "left join webpages_Roles r on r.RoleId=ur.RoleId " +
                                  "where LOWER(u.UserName) like @p0 AND r.RoleName like 'Admin'";

                users = context.UserProfile.SqlQuery(sql, "%" + searchText.ToLower() + "%").AsQueryable();
            }
            try
            {
         
                if (pageSize == 0)
                {
                    
                    users = users.OrderBy(sorting);
                }
                else
                {
                    users = users.OrderBy(sorting).Skip(startIndex).Take(pageSize);
                }

                List<Entities.UserProfile> allUser = Mapper.Map<List<Entities.UserProfile>>(users.ToList());
                return allUser;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
