//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NetShop.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class Images
    {
        public int IDimage { get; set; }
        public int ProductID { get; set; }
        public string Path { get; set; }
        public bool IsActive { get; set; }
        public bool IsMain { get; set; }
    
        public virtual Products Products { get; set; }
    }
}
