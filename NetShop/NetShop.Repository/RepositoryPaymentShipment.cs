﻿using NetShop.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Entities = NetShop.Contracts.Class;
using System.Linq.Dynamic;
using AutoMapper;
using System.Data.SqlClient;
using System.Data.Entity;

namespace NetShop.Repository
{
    public class RepositoryPaymentShipment : IRepositoryPaymentShipment
    {
        NetShopEntities context = new NetShopEntities();
        public RepositoryPaymentShipment()
        {
            Mapper.CreateMap<Entities.PaymentShipment, PaymentShipment>();
            Mapper.CreateMap<PaymentShipment, Entities.PaymentShipment>().ForMember(dest => dest.PaymentName, opt => opt.MapFrom(src => src.PaymentTypes.PaymentName))
                                                                         .ForMember(dest => dest.ShipmentName, opt => opt.MapFrom(src => src.ShipmentTypes.ShipmentName)); 
        }
        public int Add(Entities.PaymentShipment addItem)
        {
            PaymentShipment ps = Mapper.Map<PaymentShipment>(addItem);
            try
            {
                context.PaymentShipment.Add(ps);
                context.SaveChanges();

                return ps.IDpaymentShipment;
            }
            catch (SqlException ex)
            {
                return -1;
            }
        }

        public bool Delete(int deleteItem)
        {
            var ps = context.PaymentShipment.First(x => x.IDpaymentShipment == deleteItem);
            try
            {
                context.PaymentShipment.Remove(ps);
                context.Entry(ps).State = EntityState.Deleted;
                context.SaveChanges();

                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }
        }

        public Entities.PaymentShipment Get(int itemID)
        {
            PaymentShipment ps = new PaymentShipment();
            try
            {
                ps = context.PaymentShipment.Where(x => x.IDpaymentShipment == itemID).FirstOrDefault();
            }
            catch (SqlException ex)
            {
                return new Entities.PaymentShipment();
            }
            Entities.PaymentShipment newPaymentShipment = Mapper.Map<Entities.PaymentShipment>(ps);
            return newPaymentShipment;
        }

        public Entities.PaymentsShipments Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDpaymentShipment", string searchText = "")
        {
            var paymentshipments = (from ps in context.PaymentShipment
                                    select ps);

            if (!string.IsNullOrEmpty(searchText))
            {
                paymentshipments = (from ps in paymentshipments
                                    where ps.PaymentTypes.PaymentName.ToLower().Contains(searchText.ToLower()) ||
                                          ps.ShipmentTypes.ShipmentName.ToLower().Contains(searchText.ToLower())
                                    select ps);
            }

            int count = paymentshipments.Count();
            if (jtPageSize == 0)
            {
                paymentshipments = paymentshipments.OrderBy(jtSorting);
            }
            else
            {
                paymentshipments = paymentshipments.OrderBy(jtSorting).Skip(jtStartIndex).Take(jtPageSize);
            }

            Entities.PaymentsShipments mainPaymentShipments = new Entities.PaymentsShipments
            {
                TotalCount = count,
                PaymentShipmentList = Mapper.Map<List<PaymentShipment>, List<Entities.PaymentShipment>>(paymentshipments.ToList())
            };

            return mainPaymentShipments;
        }

        public bool Update(Entities.PaymentShipment updateItem)
        {
            PaymentShipment ps = Mapper.Map<PaymentShipment>(updateItem);
            if (ps != null)
            {
                try
                {
                    context.Entry(ps).State = EntityState.Modified;
                    context.SaveChanges();
                }
                catch (SqlException ex)
                {
                    return false;
                }
            }
            return true;
        }

        public bool PaymentShipmentExists(int idPayment, int idShipment)
        {
            try
            {
                return context.PaymentShipment.Any(x => x.PaymentID == idPayment && x.ShipmentID == idShipment);
            }
            catch (SqlException ex)
            {
                return false;
            }
        }
    }
}
