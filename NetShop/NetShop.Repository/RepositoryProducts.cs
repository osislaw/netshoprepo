﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetShop.Contracts;
using Entities = NetShop.Contracts.Class;
using AutoMapper;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Linq.Dynamic;

namespace NetShop.Repository
{
    public class RepositoryProducts : IRepositoryProducts
    {
        NetShopEntities context = new NetShopEntities();

        public RepositoryProducts()
        {
            Mapper.CreateMap<Entities.Product, Products>();
            Mapper.CreateMap<Products, Entities.Product>().ForMember(dest => dest.Unit, opt => opt.MapFrom(src => src.Units.Name));
            Mapper.CreateMap<Entities.Image, Images>();
            Mapper.CreateMap<Images, Entities.Image>();
        }

        public int Add(Entities.Product product)
        {
            Products products = Mapper.Map<Products>(product);
            try
            {
                context.Products.Add(products);
                context.SaveChanges();

                return products.IDproduct;
            }
            catch (SqlException ex)
            {
                return -1;
            }
        }

        public bool Update(Entities.Product product)
        {
            Products prod = Mapper.Map<Products>(product);
            if (prod != null)
            {
                try
                {
                    context.Entry(prod).State = EntityState.Modified;
                    context.SaveChanges();
                }
                catch (SqlException ex)
                {
                    return false;
                }
            }
            return true;
        }

        public bool Delete(int productID)
        {
            var product = context.Products.First(x => x.IDproduct == productID);
            try
            {
                product.IsDeleted = true;
                context.Entry(product).State = EntityState.Modified;
                context.SaveChanges();

                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }
        }

        public Entities.Product Get(int productID)
        {
            Products product = new Products();
            try
            {
                product = context.Products.Where(x => x.IDproduct == productID).FirstOrDefault();
            }
            catch (SqlException ex)
            {
            }
            Entities.Product newProduct = Mapper.Map<Entities.Product>(product);
            return newProduct;
        }

        public Entities.Products Search(int startIndex, int pageSize, string sorting = "IDproduct DESC", string searchText = "")
        {
            var products = (from product in context.Products
                            where product.IsDeleted == false
                            select product);

            if (!string.IsNullOrEmpty(searchText))
            {
                products = (from prod in products
                            where prod.Description.ToLower().Contains(searchText.ToLower())
                            || prod.ProducerCode.ToLower().Contains(searchText.ToLower())
                            || prod.ProductName.ToLower().Contains(searchText.ToLower())
                            select prod);
            }

            int count = products.Count();
            if (pageSize == 0)
            {
                products = products.OrderBy(sorting);
            }
            else
            {
                products = products.OrderBy(sorting).Skip(startIndex).Take(pageSize);
            }

            Entities.Products mainProduct = new Entities.Products
            {
                TotalCount = count,
                ProductList = Mapper.Map<List<Products>, List<Entities.Product>>(products.ToList())
            };

            return mainProduct;
        }

        public Entities.Images GetProductImages(int productID)
        {
            var images = (from image in context.Images
                          where image.ProductID == productID
                          select image);

            int count = images.Count();
            Entities.Images imageList = new Entities.Images
            {
                TotalCount = count,
                ImageList = Mapper.Map<List<Entities.Image>>(images.ToList())
            };

            return imageList;
        }

        public Entities.Image GetMainImage(int productID)
        {
            var images = (from image in context.Images
                          where image.ProductID == productID
                          && image.IsMain == true
                          select image);

            Entities.Image newImage = Mapper.Map<Entities.Image>(images.FirstOrDefault());

            return newImage;
        }

        public int AddImages(Entities.Image image)
        {
            Images img = Mapper.Map<Images>(image);
            try
            {
                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        if (img.IsMain)
                        {
                            context.UpdateMainImagesInProduct(img.ProductID);
                        }                           

                        context.Images.Add(img);
                        context.SaveChanges();

                        dbContextTransaction.Commit();
                        return img.IDimage;
                    }
                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                        return -1;
                    }
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public Entities.Image GetImage(int imageID)
        {
            Images image = new Images();
            try
            {
                image = context.Images.Where(x => x.IDimage == imageID).FirstOrDefault();
            }
            catch (SqlException ex)
            {
                return null;
            }
            Entities.Image newImage = Mapper.Map<Entities.Image>(image);
            return newImage;
        }

        public bool DeleteImage(int imageID)
        {
            var image = context.Images.First(x => x.IDimage == imageID);
            try
            {
                image.IsActive = false;
                context.Entry(image).State = EntityState.Modified;
                context.SaveChanges();

                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }
        }

        public bool UpdateImages(Entities.Image image)
        {
            Images img = Mapper.Map<Images>(image);
            if (img != null)
            {
                try
                {
                    using (var dbContextTransaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            if (img.IsMain)
                            {
                                context.UpdateMainImagesInProduct(img.ProductID);
                            }
                            img = (from images in context.Images
                                   where images.IDimage == img.IDimage
                                   select images).FirstOrDefault();
                            img.IsActive = image.IsActive;
                            img.IsMain = image.IsMain;
                            
                            context.Entry(img).State = EntityState.Modified;
                            context.SaveChanges();

                            dbContextTransaction.Commit();
                            return true;
                        }
                        catch (Exception ex)
                        {
                            dbContextTransaction.Rollback();
                            return false;
                        }
                    }
                }
                catch(Exception exc)
                {
                    return false;
                }
            }
            return false;
        }

        public Entities.Products GetCategoryProducts(int categoryID, int pageSize, int pageNumber, string orderBy)
        {
            var products = (from product in context.Products
                            where product.IsDeleted == false
                            && product.CategoryID == categoryID
                            select product);
            int totalCount = products.Count();
            products = products.OrderBy(orderBy).Skip(pageSize * pageNumber).Take(pageSize);

            Entities.Products productsAll = new Entities.Products();
            productsAll.ProductList = Mapper.Map<List<Products>, List<Entities.Product>>(products.ToList());
            productsAll.TotalCount = totalCount;
            foreach (Entities.Product product in productsAll.ProductList)
            {
                product.ProductImage = GetMainImage(product.IDproduct);
            }

            return productsAll;
        }
    }
}
