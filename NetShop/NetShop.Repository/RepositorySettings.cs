﻿using NetShop.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetShop.Contracts.Class;
using AutoMapper;
using Entities = NetShop.Contracts.Class;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Linq.Dynamic;

namespace NetShop.Repository
{
    public class RepositorySettings : IRepositorySettings
    {
        NetShopEntities context = new NetShopEntities();
        public RepositorySettings()
        {
            Mapper.CreateMap<Entities.Setting, Settings>();
            Mapper.CreateMap<Settings, Entities.Setting>().ForMember(dest => dest.ValueName, opt => opt.MapFrom(src => src.ValueTypes.Type));
        }
        public int Add(Entities.Setting addItem)
        {
            Settings settings = Mapper.Map<Settings>(addItem);
            try
            {
                context.Settings.Add(settings);
                context.SaveChanges();

                return settings.IDsetting;
            }
            catch (SqlException ex)
            {
                return -1;
            }
        }

        public bool Delete(int deleteItem)
        {
            var setting = context.Settings.First(x => x.IDsetting == deleteItem);
            try
            {
                setting.IsDeleted = true;
                context.Entry(setting).State = EntityState.Modified;
                context.SaveChanges();

                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }
        }

        public Entities.Setting Get(int itemID)
        {
            Settings setting = new Settings();
            try
            {
                setting = context.Settings.Where(x => x.IDsetting == itemID).FirstOrDefault();
            }
            catch (SqlException ex)
            {
                return new Entities.Setting();
            }
            Entities.Setting newSetting = Mapper.Map<Entities.Setting>(setting);
            return newSetting;
        }

        public Entities.Setting GetByCode(string code)
        {
            Settings setting = new Settings();
            try
            {
                setting = context.Settings.Where(x => x.Code == code).FirstOrDefault();
            }
            catch (SqlException ex)
            {
                return new Entities.Setting();
            }
            Entities.Setting newSetting = Mapper.Map<Entities.Setting>(setting);
            return newSetting;
        }

        public Entities.Settings Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDsetting", string searchText = "")
        {
            var settings = (from setting in context.Settings
                              where setting.IsDeleted == false
                              select setting);

            if (!string.IsNullOrEmpty(searchText))
            {
                settings = (from set in settings
                              where set.Description.ToLower().Contains(searchText.ToLower()) || 
                                    set.Name.ToLower().Contains(searchText.ToLower()) ||
                                    set.Code.ToLower().Contains(searchText.ToLower()) || 
                                    set.Value.ToLower().Contains(searchText.ToLower()) ||
                                    set.Creator.ToLower().Contains(searchText.ToLower())
                              select set);
            }

            int count = settings.Count();
            if (jtPageSize == 0)
            {
                settings = settings.OrderBy(jtSorting);
            }
            else
            {
                settings = settings.OrderBy(jtSorting).Skip(jtStartIndex).Take(jtPageSize);
            }

            Entities.Settings mainSettings = new Entities.Settings
            {
                TotalCount = count,
                SettingsList = Mapper.Map<List<Settings>, List<Entities.Setting>>(settings.ToList())
            };

            return mainSettings;
        }

        public bool Update(Entities.Setting updateItem)
        {
            Settings set = Mapper.Map<Settings>(updateItem);
            if (set != null)
            {
                try
                {
                    context.Entry(set).State = EntityState.Modified;
                    context.SaveChanges();
                }
                catch (SqlException ex)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
