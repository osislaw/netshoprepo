﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetShop.Contracts;
using Entities = NetShop.Contracts.Class;
using AutoMapper;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Linq.Dynamic;

namespace NetShop.Repository
{
    public class RepositoryNews : IRepositoryNews
    {
        NetShopEntities context = new NetShopEntities();

        public RepositoryNews()
        {
            Mapper.CreateMap<Entities.NewsItem, News>();
            Mapper.CreateMap<News, Entities.NewsItem>();
        }

        public int Add(Entities.NewsItem news)
        {
            News newsItem = Mapper.Map<News>(news);
            try
            {
                newsItem.CreateDate = new DateTime(newsItem.CreateDate.Year, newsItem.CreateDate.Month,
                    newsItem.CreateDate.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                context.News.Add(newsItem);
                context.SaveChanges();

                return newsItem.IDnews;
            }
            catch (SqlException ex)
            {
                return -1;
            }
        }

        public bool Update(Entities.NewsItem news)
        {
            News newsItem = Mapper.Map<News>(news);
            if (newsItem != null)
            {
                try
                {
                    newsItem.CreateDate = new DateTime(newsItem.CreateDate.Year, newsItem.CreateDate.Month,
                        newsItem.CreateDate.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
                    context.Entry(newsItem).State = EntityState.Modified;
                    context.SaveChanges();
                }
                catch (SqlException ex)
                {
                    return false;
                }
            }
            return true;
        }

        public bool Delete(int newsID)
        {
            var news = context.News.First(x => x.IDnews == newsID);
            try
            {
                news.IsDeleted = true;
                context.Entry(news).State = EntityState.Modified;
                context.SaveChanges();

                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }
        }

        public Entities.NewsItem Get(int newsID)
        {
            News news = new News();
            try
            {
                news = context.News.Where(x => x.IDnews == newsID).FirstOrDefault();
            }
            catch (SqlException ex)
            {
            }
            Entities.NewsItem newNews = Mapper.Map<Entities.NewsItem>(news);
            return newNews;
        }

        public Entities.News Search(int startIndex, int pageSize, string sorting = "IDnews", string searchText = "")
        {
            var news = (from newsItem in context.News
                        select newsItem);

            if (!string.IsNullOrEmpty(searchText))
            {
                news = (from newItem in news
                        where newItem.Description.ToLower().Contains(searchText.ToLower())
                            || newItem.ShortDescription.ToLower().Contains(searchText.ToLower())
                            || newItem.Author.ToLower().Contains(searchText.ToLower())
                            || newItem.Title.ToLower().Contains(searchText.ToLower())
                        select newItem);
            }

            int count = news.Count();
            if (pageSize == 0)
            {
                news = news.OrderBy(sorting);
            }
            else
            {
                news = news.OrderBy(sorting).Skip(startIndex).Take(pageSize);
            }

            Entities.News mainNews = new Entities.News
            {
                TotalCount = count,
                NewsItemList = Mapper.Map<List<News>, List<Entities.NewsItem>>(news.ToList())
            };

            return mainNews;
        }
    }
}
