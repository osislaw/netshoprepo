﻿using AutoMapper;
using NetShop.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Entities = NetShop.Contracts.Class;
using System.Linq.Dynamic;

namespace NetShop.Repository
{
    public class RepositoryPaymentTypes : IRepositoryPaymentTypes
    {
        NetShopEntities context = new NetShopEntities();
        public RepositoryPaymentTypes()
        {
            Mapper.CreateMap<Entities.PaymentType, PaymentTypes>();
            Mapper.CreateMap<PaymentTypes, Entities.PaymentType>();
        }
        public int Add(Entities.PaymentType addItem)
        {
            PaymentTypes paymentType = Mapper.Map<PaymentTypes>(addItem);
            try
            {
                context.PaymentTypes.Add(paymentType);
                context.SaveChanges();

                return paymentType.IDpayment;
            }
            catch (SqlException ex)
            {
                return -1;
            }
        }

        public bool Delete(int deleteItem)
        {
            var paymentType = context.PaymentTypes.First(x => x.IDpayment == deleteItem);
            try
            {
                paymentType.IsActive = false;
                context.Entry(paymentType).State = EntityState.Modified;
                context.SaveChanges();

                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }
        }

        public Entities.PaymentType Get(int itemID)
        {
            PaymentTypes paymentType = new PaymentTypes();
            try
            {
                paymentType = context.PaymentTypes.Where(x => x.IDpayment == itemID).FirstOrDefault();
            }
            catch (SqlException ex)
            {
                return new Entities.PaymentType();
            }
            Entities.PaymentType newPaymentType = Mapper.Map<Entities.PaymentType>(paymentType);
            return newPaymentType;
        }

        public Entities.PaymentTypes Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDpayment", string searchText = "")
        {
            var paymentTypes = (from paymentType in context.PaymentTypes
                                where paymentType.IsActive == true
                                select paymentType);

            if (!string.IsNullOrEmpty(searchText))
            {
                paymentTypes = (from types in paymentTypes
                            where types.PaymentName.ToLower().Contains(searchText.ToLower()) ||
                                  types.Code.ToLower().Contains(searchText.ToLower())
                            select types);
            }

            int count = paymentTypes.Count();
            if (jtPageSize == 0)
            {
                paymentTypes = paymentTypes.OrderBy(jtSorting);
            }
            else
            {
                paymentTypes = paymentTypes.OrderBy(jtSorting).Skip(jtStartIndex).Take(jtPageSize);
            }

            Entities.PaymentTypes mainPaymentTypes = new Entities.PaymentTypes
            {
                TotalCount = count,
                PaymentTypeList = Mapper.Map<List<PaymentTypes>, List<Entities.PaymentType>>(paymentTypes.ToList())
            };

            return mainPaymentTypes;
        }

        public bool Update(Entities.PaymentType updateItem)
        {
            PaymentTypes paymentType = Mapper.Map<PaymentTypes>(updateItem);
            if (paymentType != null)
            {
                try
                {
                    context.Entry(paymentType).State = EntityState.Modified;
                    context.SaveChanges();
                }
                catch (SqlException ex)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
