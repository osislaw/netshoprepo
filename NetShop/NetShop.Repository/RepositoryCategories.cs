﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetShop.Contracts;
using Entities = NetShop.Contracts.Class;
using AutoMapper;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Linq.Dynamic;
using NetShop.Contracts.Class;

namespace NetShop.Repository
{
    public class RepositoryCategories : IRepositoryCategories
    {
        NetShopEntities context = new NetShopEntities();

        public RepositoryCategories()
        {
            Mapper.CreateMap<Entities.Category, Categories>();
            Mapper.CreateMap<Categories, Entities.Category>();
            Mapper.CreateMap<Entities.LeafCategory, Categories>();
            Mapper.CreateMap<Categories, Entities.LeafCategory>();
        }

        public int Add(Entities.Category category)
        {
            Categories categories = Mapper.Map<Categories>(category);
            try
            {
                context.Categories.Add(categories);
                context.SaveChanges();

                return categories.IDcategory;
            }
            catch (SqlException ex)
            {
                return -1;
            }
        }

        public bool Update(Entities.Category category)
        {
            Categories cat = Mapper.Map<Categories>(category);
            if (cat != null)
            {
                try
                {
                    context.Entry(cat).State = EntityState.Modified;
                    context.SaveChanges();
                }
                catch (SqlException ex)
                {
                    return false;
                }
            }
            return true;
        }

        public bool Delete(int categoryID)
        {
            var category = context.Categories.First(x => x.IDcategory == categoryID);
            try
            {
                category.IsDeleted = true;
                context.Entry(category).State = EntityState.Modified;
                context.SaveChanges();

                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }
        }

        public Entities.Category Get(int categoryID)
        {
            Categories category = new Categories();
            try
            {
                category = context.Categories.Where(x => x.IDcategory == categoryID).FirstOrDefault();
            }
            catch (SqlException ex)
            {
                return new Entities.Category();
            }
            Entities.Category newCategory = Mapper.Map<Entities.Category>(category);
            return newCategory;
        }

        public Entities.Categories Search(int startIndex, int pageSize, string sorting = "IDcategory", string searchText = "")
        {
            var categories = (from category in context.Categories
                              where category.IsDeleted == false
                              && category.Visible == true
                              select category);

            if (!string.IsNullOrEmpty(searchText))
            {
                categories = (from cat in categories
                              where cat.Description.ToLower().Contains(searchText.ToLower())
                            || cat.CategoryName.ToLower().Contains(searchText.ToLower())
                              select cat);
            }

            int count = categories.Count();
            if (pageSize == 0)
            {
                categories = categories.OrderBy(sorting);
            }
            else
            {
                categories = categories.OrderBy(sorting).Skip(startIndex).Take(pageSize);
            }

            Entities.Categories mainCategory = new Entities.Categories
            {
                TotalCount = count,
                CategoryList = Mapper.Map<List<Categories>, List<Entities.Category>>(categories.ToList())
            };

            return mainCategory;
        }

        public List<TreeCategory> GetTree()
        {
            var mainCategories = (from category in context.Categories
                                  where category.IsDeleted == false
                                  && category.ParentID == -1
                                  && category.Visible == true
                                  select new TreeCategory
                                  {
                                      CategoryName = category.CategoryName,
                                      IDCategory = category.IDcategory
                                  }).ToList();

            List<TreeCategory> treeCategory = new List<TreeCategory>();
            treeCategory = mainCategories;

            foreach (TreeCategory category in treeCategory)
            {
                var leafCategory = (from categ in context.Categories
                                    where categ.ParentID != -1
                                    && categ.ParentID == category.IDCategory
                                    && categ.IsDeleted == false
                                    && categ.Visible == true
                                    select categ);
                if (leafCategory.Count() > 0)
                {
                    category.LeafCategory = Mapper.Map<List<LeafCategory>>(leafCategory.OrderBy("CategoryName").ToList());
                }
            }

            return treeCategory;
        }

        public List<Category> GetChildrenCategory(int parentID)
        {
            var category = (from categ in context.Categories
                            where categ.ParentID != -1
                            && categ.ParentID == parentID
                            && categ.IsDeleted == false
                            && categ.Visible == true
                            select categ);

            List<Category> categories = new List<Category>();
            categories = Mapper.Map<List<Category>>(category.OrderBy("IDcategory").ToList());

            return categories;
        }

        public Entities.Categories SearchCategories(int startIndex, int pageSize, string sorting = "IDcategory", string searchText = "")
        {
            var categories = (from category in context.Categories
                              where category.IsDeleted == false
                              && category.Visible == true
                              && category.ParentID == -1
                              select category);

            if (!string.IsNullOrEmpty(searchText))
            {
                categories = (from cat in categories
                              where cat.Description.ToLower().Contains(searchText.ToLower())
                            || cat.CategoryName.ToLower().Contains(searchText.ToLower())
                              select cat);
            }

            int count = categories.Count();
            if (pageSize == 0)
            {
                categories = categories.OrderBy(sorting);
            }
            else
            {
                categories = categories.OrderBy(sorting).Skip(startIndex).Take(pageSize);
            }

            Entities.Categories mainCategory = new Entities.Categories
            {
                TotalCount = count,
                CategoryList = Mapper.Map<List<Categories>, List<Entities.Category>>(categories.ToList())
            };

            return mainCategory;
        }
    }
}
