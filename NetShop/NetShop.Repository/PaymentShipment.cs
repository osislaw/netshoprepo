//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NetShop.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class PaymentShipment
    {
        public int IDpaymentShipment { get; set; }
        public int PaymentID { get; set; }
        public int ShipmentID { get; set; }
    
        public virtual PaymentTypes PaymentTypes { get; set; }
        public virtual ShipmentTypes ShipmentTypes { get; set; }
    }
}
