﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Repository
{
    public static class SettingsNames
    {
        // czy pierwsze uruchomienie
        // ilosc newsow na stronie
        // waluta
        // nazwa sklepu
        // kontakt
        // regulamin
        // polityka plikow cookies
        // polityka zwrotow
        public const string FIRST_RUN = "FIRST_RUN";
        public const string NEWS_COUNT = "NEWS_COUNT";
        public const string CURRENCY = "CURRENCY";
        public const string SHOP_NAME = "SHOP_NAME";
        public const string CONTACT = "CONTACT";
        public const string REGULATIONS = "REGULATIONS";
        public const string COOKIES = "COOKIES";
        public const string RETURN_POLICY = "RETURN_POLICY";
    }
}
