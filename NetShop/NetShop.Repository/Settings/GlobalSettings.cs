﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Repository
{
    public class GlobalSettings
    {
        #region Prywatne zmienne
        private bool firstRun;
        private int newsCount;
        private string currency;
        private string shopName;
        private string contact;
        private string regulations;
        private string cookies;
        private string returnPolicy;
        #endregion

        #region Ustawienia - właściwości dostępowe
        public int NewsCount
        {
            get { return newsCount; }
        }
        public bool FirstRun
        {
            get { return firstRun; }
        }
        public string Currency
        {
            get { return currency; }
        }
        public string ShopName
        {
            get { return shopName; }
        }
        public string Contact
        {
            get { return contact; }
        }
        public string Regulations
        {
            get { return regulations; }
        }
        public string Cookies
        {
            get { return cookies; }
        }
        public string ReturnPolicy
        {
            get { return returnPolicy; }
        }
        #endregion

        #region Singleton

        private GlobalSettings()
        {
            Reload();
        }

        private static readonly GlobalSettings instance = new GlobalSettings();

        public static GlobalSettings Instance
        {
            get { return instance;  }
        }
        
        #endregion

        public void Reload()
        {
            firstRun = SettingsFactory.GetSettingValue<bool>(SettingsNames.FIRST_RUN);
            newsCount = SettingsFactory.GetSettingValue<int>(SettingsNames.NEWS_COUNT);
            currency = SettingsFactory.GetSettingValue<string>(SettingsNames.CURRENCY);
            shopName = SettingsFactory.GetSettingValue<string>(SettingsNames.SHOP_NAME);
            contact = SettingsFactory.GetSettingValue<string>(SettingsNames.CONTACT);
            regulations = SettingsFactory.GetSettingValue<string>(SettingsNames.REGULATIONS);
            cookies = SettingsFactory.GetSettingValue<string>(SettingsNames.COOKIES);
            returnPolicy = SettingsFactory.GetSettingValue<string>(SettingsNames.RETURN_POLICY);
        }

        public void Reload(string settingName)
        {
            switch (settingName)
            {
                case SettingsNames.FIRST_RUN:
                    firstRun = SettingsFactory.GetSettingValue<bool>(settingName);
                    break;
                case SettingsNames.NEWS_COUNT:
                    newsCount = SettingsFactory.GetSettingValue<int>(settingName);
                    break;
                case SettingsNames.CURRENCY:
                    currency = SettingsFactory.GetSettingValue<string>(settingName);
                    break;
                case SettingsNames.SHOP_NAME:
                    shopName = SettingsFactory.GetSettingValue<string>(settingName);
                    break;
                case SettingsNames.CONTACT:
                    contact = SettingsFactory.GetSettingValue<string>(settingName);
                    break;
                case SettingsNames.REGULATIONS:
                    regulations = SettingsFactory.GetSettingValue<string>(settingName);
                    break;
                case SettingsNames.COOKIES:
                    cookies = SettingsFactory.GetSettingValue<string>(settingName);
                    break;
                case SettingsNames.RETURN_POLICY:
                    returnPolicy = SettingsFactory.GetSettingValue<string>(settingName);
                    break;
                default:
                    break;
            }
        }

    }
}
