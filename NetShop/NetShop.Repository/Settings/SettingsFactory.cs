﻿using NetShop.Contracts.Class;
using NetShop.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetShop.Utils.Extensions;

namespace NetShop.Repository
{
    public static class SettingsFactory
    {
        private readonly static Dictionary<string, int> defaultIntSettings = new Dictionary<string, int>() { 
                                                                                                        { SettingsNames.NEWS_COUNT, 4 }
                                                                                                    };
        private readonly static Dictionary<string, string> defaultStringSettings = new Dictionary<string, string>()
                                                                                                            {
                                                                                                                { SettingsNames.CONTACT, String.Empty },
                                                                                                                { SettingsNames.COOKIES, String.Empty },
                                                                                                                { SettingsNames.CURRENCY, "zł" },
                                                                                                                { SettingsNames.REGULATIONS, String.Empty },
                                                                                                                { SettingsNames.RETURN_POLICY, String.Empty },
                                                                                                                { SettingsNames.SHOP_NAME, String.Empty }
                                                                                                            };
        private readonly static Dictionary<string, bool> defaultBoolSettings = new Dictionary<string, bool>()
                                                                                                            {
                                                                                                                { SettingsNames.FIRST_RUN, true}
                                                                                                            };
        public static T GetSettingValue<T>(string settingCode)
        {
            NetShop.Contracts.IRepositorySettings repository = new RepositorySettings();
            Setting setting = repository.GetByCode(settingCode);

            return ReturnByValueType<T>(setting.Value, setting.ValueName);
        }

        private static T ReturnByValueType<T>(string settingValue, string valueType)
        {
            try
            {
                switch (valueType)
                {
                    case ValueTypeNames.BOOL:
                        return (T)Convert.ChangeType(settingValue.ToBoolean(), typeof(T));
                    case ValueTypeNames.DOUBLE:
                        return (T)Convert.ChangeType(Convert.ToDouble(settingValue), typeof(T));
                    case ValueTypeNames.FLOAT:
                        return (T)Convert.ChangeType((float)Convert.ToDouble(settingValue), typeof(T));
                    case ValueTypeNames.INT:
                        return (T)Convert.ChangeType(int.Parse(settingValue), typeof(T));
                    case ValueTypeNames.STRING:
                        return (T)Convert.ChangeType(settingValue, typeof(T));
                    default:
                        throw new Exception("Zły parametr lub ustawienie");
                }
            }
            catch(Exception e)
            {
                switch (valueType)
                {
                    case ValueTypeNames.BOOL:
                        return (T)Convert.ChangeType(GetDefaultBoolSetting(settingValue), typeof(T));
                    case ValueTypeNames.INT:
                        return (T)Convert.ChangeType(GetDefaultIntSetting(settingValue), typeof(T));
                    case ValueTypeNames.STRING:
                        return (T)Convert.ChangeType(GetDefaultStringSetting(settingValue), typeof(T));
                    default:
                        throw new Exception("Zły parametr lub ustawienie");
                }
            }
        }

        private static string GetDefaultStringSetting(string settingCode)
        {
            try
            {
                return defaultStringSettings[settingCode];
            }
            catch(KeyNotFoundException ex)
            {
                return String.Empty;
            }
        }

        private static bool GetDefaultBoolSetting(string settingCode)
        {
            try
            {
                return defaultBoolSettings[settingCode];
            }
            catch (KeyNotFoundException ex)
            {
                return false;
            }
        }

        private static int GetDefaultIntSetting(string settingCode)
        {
            try
            {
                return defaultIntSettings[settingCode];
            }
            catch (KeyNotFoundException ex)
            {
                return -1;
            }
        }

    }
}
