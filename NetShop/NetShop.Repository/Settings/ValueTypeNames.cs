﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Repository
{
    public static class ValueTypeNames
    {
        public const string INT = "INT";
        public const string STRING = "STRING";
        public const string BOOL = "BOOL";
        public const string FLOAT = "FLOAT";
        public const string DOUBLE = "DOUBLE";
    }
}
