﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetShop.Contracts;
using Entities = NetShop.Contracts.Class;
using AutoMapper;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Linq.Dynamic;

namespace NetShop.Repository
{
    public class RepositoryCustomers : IRepositoryCustomers
    {
        NetShopEntities context = new NetShopEntities();

        public RepositoryCustomers()
        {
            Mapper.CreateMap<Entities.Customer, Customer>();
            Mapper.CreateMap<Customer, Entities.Customer>().ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserProfile.UserName));
        }

        public int Add(Entities.Customer customer)
        {
            Customer cstmr = Mapper.Map<Customer>(customer);
            try
            {
                context.Customer.Add(cstmr);
                context.SaveChanges();

                return cstmr.IDcustomer;
            }
            catch (SqlException ex)
            {
                return -1;
            }
        }

        public bool Update(Entities.Customer customer)
        {
            Customer cstmr = Mapper.Map<Customer>(customer);
            if (cstmr != null)
            {
                try
                {
                    context.Entry(cstmr).State = EntityState.Modified;
                    context.SaveChanges();
                }
                catch (SqlException ex)
                {
                    return false;
                }
            }
            return true;
        }

        public bool Delete(int customerID)
        {
            var customer = context.Customer.First(x => x.IDcustomer == customerID);
            try
            {
                customer.IsDeleted = true;
                context.Entry(customer).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }
        }

        public Entities.Customer Get(int customerID)
        {
            Customer customer = new Customer();
            try
            {
                customer = context.Customer.Include(x => x.UserProfile).Where(x => x.IDcustomer == customerID).FirstOrDefault();
            }
            catch (SqlException ex)
            {
            }
            Entities.Customer newCustomer = Mapper.Map<Entities.Customer>(customer);
            return newCustomer;
        }

        public Entities.Customers Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDcustomer", string searchText = "")
        {
            var customers = (from customer in context.Customer
                             where customer.IsDeleted == false
                             select customer);


            if (!string.IsNullOrEmpty(searchText))
            {
                customers = (from cstmr in customers
                            where cstmr.City.ToLower().Contains(searchText.ToLower())
                            || cstmr.FirstName.ToLower().Contains(searchText.ToLower())
                            || cstmr.Surname.ToLower().Contains(searchText.ToLower())
                            || cstmr.Street.ToLower().Contains(searchText.ToLower())
                            || cstmr.CompanyNIP.ToLower().Contains(searchText.ToLower())
                            || cstmr.CompanyCity.ToLower().Contains(searchText.ToLower())
                            || cstmr.CompanyStreet.ToLower().Contains(searchText.ToLower())
                            || cstmr.CompanyZipCode.ToLower().Contains(searchText.ToLower())
                            || cstmr.UserProfile.UserName.ToLower().Contains(searchText.ToLower())
                            select cstmr);
            }

            int count = customers.Count();
            if (jtPageSize == 0)
            {
                customers = customers.OrderBy(jtSorting);
            }
            else
            {
                customers = customers.OrderBy(jtSorting).Skip(jtStartIndex).Take(jtPageSize);
            }

            Entities.Customers mainCustomer = new Entities.Customers
            {
                TotalCount = count,
                CustomerList = Mapper.Map<List<Customer>, List<Entities.Customer>>(customers.ToList())
            };

            return mainCustomer;
        }

        public Entities.Customer Get(string userName)
        {
            Customer customer = new Customer();
            try
            {
                customer = context.Customer.Include(x => x.UserProfile).Where(x => x.UserProfile.UserName == userName).FirstOrDefault();
            }
            catch (SqlException ex)
            {
            }
            Entities.Customer newCustomer = Mapper.Map<Entities.Customer>(customer);
            return newCustomer;
        }
    }
}
