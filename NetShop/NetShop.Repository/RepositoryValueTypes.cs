﻿using NetShop.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetShop.Contracts.Class;
using AutoMapper;
using Entities = NetShop.Contracts.Class;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Linq.Dynamic;

namespace NetShop.Repository
{
    public class RepositoryValueTypes : IRepositoryValueTypes
    {
        NetShopEntities context = new NetShopEntities();
        public RepositoryValueTypes()
        {
            Mapper.CreateMap<Entities.ValueType, ValueTypes>();
            Mapper.CreateMap<ValueTypes, Entities.ValueType>();
        }
        public int Add(Entities.ValueType addItem)
        {
            ValueTypes valueTypes = Mapper.Map<ValueTypes>(addItem);
            try
            {
                context.ValueTypes.Add(valueTypes);
                context.SaveChanges();

                return valueTypes.IDvalueType;
            }
            catch (SqlException ex)
            {
                return -1;
            }
        }

        public bool Delete(int deleteItem)
        {
            var valueType = context.ValueTypes.First(x => x.IDvalueType == deleteItem);
            try
            {
                valueType.IsDeleted = true;
                context.Entry(valueType).State = EntityState.Modified;
                context.SaveChanges();

                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }
        }

        public Entities.ValueType Get(int itemID)
        {
            ValueTypes valueType = new ValueTypes();
            try
            {
                valueType = context.ValueTypes.Where(x => x.IDvalueType == itemID).FirstOrDefault();
            }
            catch (SqlException ex)
            {
                return new Entities.ValueType();
            }
            Entities.ValueType newValueType = Mapper.Map<Entities.ValueType>(valueType);
            return newValueType;
        }

        public Entities.ValueTypes Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDvalueType", string searchText = "")
        {
            var valueTypes = (from valueType in context.ValueTypes
                            where valueType.IsDeleted == false
                            select valueType);

            if (!string.IsNullOrEmpty(searchText))
            {
                valueTypes = (from val in valueTypes
                              where val.Description.ToLower().Contains(searchText.ToLower()) ||
                                    val.Type.ToLower().Contains(searchText.ToLower()) ||
                                    val.Creator.ToLower().Contains(searchText.ToLower())
                              select val);
            }

            int count = valueTypes.Count();
            if (jtPageSize == 0)
            {
                valueTypes = valueTypes.OrderBy(jtSorting);
            }
            else
            {
                valueTypes = valueTypes.OrderBy(jtSorting).Skip(jtStartIndex).Take(jtPageSize);
            }

            Entities.ValueTypes mainValueTypes = new Entities.ValueTypes
            {
                TotalCount = count,
                ValueTypeList = Mapper.Map<List<ValueTypes>, List<Entities.ValueType>>(valueTypes.ToList())
            };

            return mainValueTypes;
        }

        public bool Update(Entities.ValueType updateItem)
        {
            ValueTypes val = Mapper.Map <ValueTypes>(updateItem);
            if (val != null)
            {
                try
                {
                    context.Entry(val).State = EntityState.Modified;
                    context.SaveChanges();
                }
                catch (SqlException ex)
                {
                    return false;
                }
            }
            return true;
        }
    }
    
}
