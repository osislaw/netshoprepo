﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetShop.Contracts;
using Entities = NetShop.Contracts.Class;
using AutoMapper;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Linq.Dynamic;

namespace NetShop.Repository
{
    public class RepositoryRealizations : IRepositoryRealizations
    {
        NetShopEntities context = new NetShopEntities();

        public RepositoryRealizations()
        {
            Mapper.CreateMap<Entities.Realization, Realizations>();
            Mapper.CreateMap<Realizations, Entities.Realization>();
            Mapper.CreateMap<Entities.Basket, Baskets>();
            Mapper.CreateMap<Baskets, Entities.Basket>();
            Mapper.CreateMap<Entities.RealizationAddress, RealizationAddress>();
            Mapper.CreateMap<RealizationAddress, Entities.RealizationAddress>();
            Mapper.CreateMap<Entities.RealizationItem, RealizationItem>();
            Mapper.CreateMap<RealizationItem, Entities.RealizationItem>();
            Mapper.CreateMap<Entities.RealizationStatus, RealizationStatus>();
            Mapper.CreateMap<RealizationStatus, Entities.RealizationStatus>();


            //dodatkowe mapowania
            Mapper.CreateMap<Realizations, Entities.Realization>().ForMember(dest => dest.StatusName, opt => opt.MapFrom(src => src.RealizationStatus.StatusName));
            Mapper.CreateMap<Realizations, Entities.Realization>().ForMember(dest => dest.ShipmentName, opt => opt.MapFrom(src => src.ShipmentTypes.ShipmentName));
            Mapper.CreateMap<Realizations, Entities.Realization>().ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.Customer.Surname + " " + src.Customer.FirstName + " " + src.Customer.CompanyName));
        }

        public int Add(Entities.Realization realization)
        {
            Realizations real = Mapper.Map<Realizations>(realization);
            try
            {
                context.Realizations.Add(real);
                context.SaveChanges();

                return real.IDrealization;
            }
            catch (SqlException ex)
            {
                return -1;
            }
        }

        public bool Update(Entities.Realization realization)
        {
            Realizations realiz = Mapper.Map<Realizations>(realization);
            if (realiz != null)
            {
                Realizations real = (from r in context.Realizations
                                     where r.IDrealization == realization.IDrealization
                                     select r).FirstOrDefault();

                real.RealizationStatusID = realization.RealizationStatusID;
                real.CustomerID = realization.CustomerID;
                real.Comment = realization.Comment;
                real.CreateDate = realization.CreateDate;
                real.EndDate = realization.EndDate;
                real.PaymentID = realization.PaymentID;
                real.ShipmentID = realization.ShipmentID;

                using (var dbContextTransaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        context.Entry(real).State = EntityState.Modified;
                        context.SaveChanges();

                        context.UpdateRealizationValue(realization.IDrealization);
                        context.SaveChanges();

                        dbContextTransaction.Commit();
                        context.Entry(real).Reload();
                    }
                    catch (SqlException ex)
                    {
                        dbContextTransaction.Rollback();
                        return false;
                    }
                }
            }
            return true;
        }

        public bool Delete(int realizationID)
        {
            try
            {
                Realizations real = (from r in context.Realizations
                                     where r.IDrealization == realizationID
                                     select r).FirstOrDefault();
                real.IsDeleted = true;

                context.Entry(real).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Entities.Realization Get(int realizationID)
        {
            Realizations realization = new Realizations();
            try
            {
                realization = context.Realizations.Where(x => x.IDrealization == realizationID).FirstOrDefault();
            }
            catch (SqlException ex)
            {
            }
            Entities.Realization newRealization = Mapper.Map<Entities.Realization>(realization);
            newRealization.PaymentName = (from payment in context.PaymentTypes
                                          where payment.IDpayment == newRealization.PaymentID
                                          select payment.PaymentName).FirstOrDefault();
            newRealization.ShipmentName = (from shipment in context.ShipmentTypes
                                           where shipment.IDshipment == newRealization.ShipmentID
                                           select shipment.ShipmentName).FirstOrDefault();
            return newRealization;
        }

        public Entities.Realizations Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDrealization", string searchText = "")
        {
            var realizations = (from real in context.Realizations
                                where real.IsDeleted == false
                                select real);

            if (!string.IsNullOrEmpty(searchText))
            {
                realizations = (from real in realizations
                                where real.PaymentTypes.PaymentName.ToLower().Contains(searchText.ToLower())
                             || real.ShipmentTypes.ShipmentName.ToLower().Contains(searchText.ToLower())
                             || real.Customer.CompanyName.ToLower().Contains(searchText.ToLower())
                             || real.Customer.FirstName.ToLower().Contains(searchText.ToLower())
                             || real.Customer.Surname.ToLower().Contains(searchText.ToLower())
                             || real.Customer.City.ToLower().Contains(searchText.ToLower())
                             || real.Customer.CompanyCity.ToLower().Contains(searchText.ToLower())
                             || real.Customer.Email.ToLower().Contains(searchText.ToLower())
                             || real.Customer.Phone.ToLower().Contains(searchText.ToLower())
                                select real);
            }

            int count = realizations.Count();
            if (jtPageSize == 0)
            {
                realizations = realizations.OrderBy(jtSorting);
            }
            else
            {
                realizations = realizations.OrderBy(jtSorting).Skip(jtStartIndex).Take(jtPageSize);
            }

            Entities.Realizations mainRealizations = new Entities.Realizations
            {
                TotalCount = count,
                RealizationList = Mapper.Map<List<Realizations>, List<Entities.Realization>>(realizations.ToList())
            };

            foreach (Entities.Realization realiz in mainRealizations.RealizationList)
            {
                realiz.PaymentName = (from payment in context.PaymentTypes
                                      where payment.IDpayment == realiz.PaymentID
                                      select payment.PaymentName).FirstOrDefault();
            }

            return mainRealizations;

        }

        public bool AddToBasket(Entities.Basket basket)
        {
            Baskets basketItem = Mapper.Map<Baskets>(basket);
            if (basketItem != null)
            {
                try
                {
                    if (context.Baskets.Where(x => x.ProductID == basket.ProductID && x.SessionID == basket.SessionID).Count() > 0)
                    {
                        Baskets existItem = context.Baskets.Where(x => x.ProductID == basket.ProductID && x.SessionID == basket.SessionID).FirstOrDefault();
                        existItem.ProductCount += basket.ProductCount;
                        context.Entry(existItem).State = EntityState.Modified;
                        context.SaveChanges();
                    }
                    else
                    {
                        context.Baskets.Add(basketItem);
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        public void RemoveItemsFromBasket(string sesssionID)
        {
            try
            {
                context.Baskets.RemoveRange(context.Baskets.Where(x => x.SessionID == sesssionID));
                context.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }

        public List<Entities.Basket> GetBasket(string sessionID)
        {
            try
            {
                var basket = (from baskets in context.Baskets
                              where baskets.SessionID == sessionID
                              select baskets).ToList();

                List<Entities.Basket> basketItems = Mapper.Map<List<Entities.Basket>>(basket);
                foreach (Entities.Basket bask in basketItems)
                {
                    var product = (from prod in context.Products
                                   where prod.IDproduct == bask.ProductID
                                   select prod).FirstOrDefault();
                    bask.ProductName = product.ProductName;
                    bask.PriceBrutto = product.PriceBrutto;
                    if (product.UnitID > 0)
                    {
                        bask.Unit = product.Units.Name;
                    }
                }
                return basketItems;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool RemoveItemFromBasket(string sesssionID, int productID)
        {
            try
            {
                var basket = (from bask in context.Baskets
                              where bask.SessionID == sesssionID
                              && bask.ProductID == productID
                              select bask).FirstOrDefault();
                if (basket != null)
                {
                    context.Baskets.Remove(basket);
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public decimal UpdateProductCount(string sessionID, int productID, int productCount)
        {
            try
            {
                var basket = (from bask in context.Baskets
                              where bask.SessionID == sessionID
                              && bask.ProductID == productID
                              select bask).FirstOrDefault();
                if (basket != null)
                {
                    basket.ProductCount = productCount;
                    context.Entry(basket).State = EntityState.Modified;
                    context.SaveChanges();

                    var productPrice = (from prod in context.Products
                                        where prod.IDproduct == productID
                                        select prod.PriceBrutto).FirstOrDefault();

                    return Convert.ToDecimal(productPrice * productCount);
                }
            }
            catch (Exception ex)
            {

            }
            return -1.0m;
        }

        public int AddAddress(Entities.RealizationAddress address)
        {
            RealizationAddress realAddress = Mapper.Map<RealizationAddress>(address);
            try
            {
                context.RealizationAddress.Add(realAddress);
                context.SaveChanges();

                return realAddress.IDrealizationAddress;
            }
            catch (SqlException ex)
            {
                return -1;
            }
        }

        public void AddItemsToRealization(string sessionID, int realizationID)
        {
            var items = (from basket in context.Baskets
                         where basket.SessionID == sessionID
                         select basket);

            foreach (var item in items)
            {
                var product = (from prod in context.Products
                               where prod.IDproduct == item.ProductID
                               select prod).FirstOrDefault();
                for (int i = 0; i < item.ProductCount; i++)
                {
                    RealizationItem realizationitem = new RealizationItem();
                    realizationitem.IsDeleted = false;
                    realizationitem.ProductID = item.ProductID;
                    realizationitem.ProductName = product.ProductName;
                    realizationitem.ProductPrice = product.PriceNetto;
                    realizationitem.ProductPriceBrutto = product.PriceBrutto;
                    realizationitem.RealizationID = realizationID;
                    realizationitem.RealizationItemStatusID = (int)Entities.ItemStatus.LackOfProduct;
                    context.RealizationItem.Add(realizationitem);
                    context.SaveChanges();
                }
            }
        }

        public int AddRealization(Entities.Realization realization, Entities.RealizationAddress address, string sessionID)
        {
            using (var dbContextTransaction = context.Database.BeginTransaction())
            {
                try
                {
                    realization.RealizationAddressID = AddAddress(address);

                    realization.IDrealization = Add(realization);

                    AddItemsToRealization(sessionID, realization.IDrealization);

                    context.UpdateRealizationValue(realization.IDrealization);

                    dbContextTransaction.Commit();
                    return realization.IDrealization;
                }
                catch (Exception)
                {
                    dbContextTransaction.Rollback();
                    return -1;
                }
            }
        }

        public Entities.RealizationResult GetRealizationResult(int realizationID)
        {
            Entities.RealizationResult realization = new Entities.RealizationResult();
            try
            {
                realization = (from real in context.Realizations
                               where real.IDrealization == realizationID
                               select new Entities.RealizationResult
                               {
                                   RealizationID = real.IDrealization,
                                   TotalAmountBrutto = real.TotalAmountBrutto,
                                   Shipment = real.ShipmentTypes.ShipmentName,
                                   Payment = real.PaymentTypes.PaymentName
                               }).FirstOrDefault();
            }
            catch (SqlException ex)
            {
            }
            return realization;
        }

        public Entities.Realizations GetByCustomer(int customerID, int pageNumber, int pageSize)
        {
            var real = (from realization in context.Realizations
                        where realization.IsDeleted == false
                        && realization.CustomerID == customerID
                        select realization);

            int totalCount = real.Count();
            Entities.Realizations realizations = new Entities.Realizations
            {
                TotalCount = totalCount,
                RealizationList = Mapper.Map<List<Entities.Realization>>(real.OrderBy("IDrealization DESC").Skip(pageNumber * pageSize).Take(pageSize)).ToList()
            };

            return realizations;
        }

        public List<Entities.Order> GetItems(int realizationID)
        {
            List<Entities.Order> orderList = new List<Entities.Order>();
            var items = (from order in context.RealizationItem
                         where order.IsDeleted == false
                         && order.RealizationID == realizationID
                         select order).ToList();

            var itemList = (from order in context.RealizationItem
                            where order.IsDeleted == false
                            && order.RealizationID == realizationID
                            select order).ToList();

            var tmpList = itemList.GroupBy(x => x.ProductPriceBrutto).Distinct().ToList();

            List<RealizationItem> realItemList = new List<RealizationItem>();
            foreach (var tmpItem in tmpList)
            {
                realItemList.Add(tmpItem.First());
            }

            {
                foreach (var it in realItemList)
                {
                    var orders = (from item in context.RealizationItem
                                  where item.ProductID == it.ProductID
                                  && item.RealizationID == realizationID
                                  && item.IDrealizationItem == it.IDrealizationItem
                                  select new Entities.Order
                                  {
                                      IDrealizationItem = item.IDrealizationItem,
                                      OrderedCount = (from ite in context.RealizationItem
                                                      where ite.ProductPriceBrutto == item.ProductPriceBrutto
                                                      && ite.ProductID == item.ProductID
                                                      && ite.RealizationID == item.RealizationID
                                                      && ite.IsDeleted == false
                                                      select ite.ProductID).Count(),
                                      ProductID = item.ProductID,
                                      ProductName = item.ProductName,
                                      ProductPrice = item.ProductPrice,
                                      ProductPriceBrutto = item.ProductPriceBrutto,
                                      RealizationID = item.RealizationID,
                                      RealizationItemStatusID = item.RealizationItemStatusID,
                                      Unit = (item.Products.UnitID != null) ? item.Products.Units.Name : string.Empty
                                  }).FirstOrDefault();
                    orderList.Add(orders);
                }

                return orderList;
            }
        }

        public Entities.RealizationItems GetRealizationItems(int realizationID)
        {
            try
            {
                var realItems = (from real in context.RealizationItem
                                 where real.RealizationID == realizationID
                                 && real.IsDeleted == false
                                 select real);
                int totalCount = realItems.Count();
                Entities.RealizationItems realizationItems = new Entities.RealizationItems
                {
                    TotalCount = totalCount,
                    RealizationItemList = Mapper.Map<List<Entities.RealizationItem>>(realItems).ToList()
                };
                return realizationItems;
            }
            catch (SqlException ex)
            {
                return new Entities.RealizationItems();
            }
        }

        public List<Entities.RealizationStatus> GetStatusList()
        {
            var status = (from stat in context.RealizationStatus
                          select new Entities.RealizationStatus
                          {
                              RealizationStatusID = stat.IDrealizationStatus,
                              StatusName = stat.StatusName
                          }).ToList();
            //List<Entities.RealizationStatus> statusList = Mapper.Map<List<Entities.RealizationStatus>>(status);

            return status;
        }

        public Entities.RealizationItem AddItemToRealization(Entities.RealizationItem realizationItem)
        {
            RealizationItem item = new RealizationItem();

            var product = (from products in context.Products
                           where products.IDproduct == realizationItem.ProductID
                           select products).FirstOrDefault();

            if (product != null)
            {
                item.IsDeleted = false;
                item.ProductName = product.ProductName;
                item.ProductPrice = Math.Round(realizationItem.ProductPriceBrutto / 1.23m, 2);
                item.ProductPriceBrutto = realizationItem.ProductPriceBrutto;
                item.RealizationID = realizationItem.RealizationID;
                item.RealizationItemStatusID = 1;
                item.ProductID = realizationItem.ProductID;

                try
                {
                    context.RealizationItem.Add(item);
                    context.SaveChanges();

                    context.UpdateRealizationValue(realizationItem.RealizationID);
                    context.SaveChanges();

                    var realization = (from real in context.Realizations
                                       where real.IDrealization == item.RealizationID
                                       select real).FirstOrDefault();
                    if (realization != null)
                    {
                        context.Entry(realization).Reload();
                    }

                    Entities.RealizationItem realItem = Mapper.Map<Entities.RealizationItem>(item);
                    return realItem;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            else
            {
                return null;
            }

        }

        public Entities.RealizationItem UpdateItemInRealization(Entities.RealizationItem realizationItem)
        {
            try
            {
                var item = (from items in context.RealizationItem
                            where items.IDrealizationItem == realizationItem.IDrealizationItem
                            select items).FirstOrDefault();
                if (item != null)
                {
                    item.ProductPriceBrutto = realizationItem.ProductPriceBrutto;
                    item.ProductPrice = Math.Round(realizationItem.ProductPriceBrutto / 1.23m, 2);

                    context.Entry(item).State = EntityState.Modified;
                    context.SaveChanges();

                    context.UpdateRealizationValue(realizationItem.RealizationID);
                    context.SaveChanges();

                    var realization = (from real in context.Realizations
                                       where real.IDrealization == item.RealizationID
                                       select real).FirstOrDefault();
                    if (realization != null)
                    {
                        context.Entry(realization).Reload();
                    }

                    realizationItem = Mapper.Map<Entities.RealizationItem>(item);
                    return realizationItem;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool DeleteItemFromRealization(int realizationItemID)
        {
            try
            {
                var item = (from realizationItem in context.RealizationItem
                            where realizationItem.IDrealizationItem == realizationItemID
                            select realizationItem).FirstOrDefault();
                if (item != null)
                {
                    item.IsDeleted = true;

                    context.Entry(item).State = EntityState.Modified;
                    context.SaveChanges();

                    context.UpdateRealizationValue(item.RealizationID);
                    context.SaveChanges();

                    var realization = (from real in context.Realizations
                                       where real.IDrealization == item.RealizationID
                                       select real).FirstOrDefault();
                    if(realization != null)
                    {
                        context.Entry(realization).Reload();
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
