﻿using AutoMapper;
using NetShop.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Entities = NetShop.Contracts.Class;
using System.Linq.Dynamic;

namespace NetShop.Repository
{
    public class RepositoryShipmentTypes : IRepositoryShipmentTypes
    {
        NetShopEntities context = new NetShopEntities();
        public RepositoryShipmentTypes()
        {
            Mapper.CreateMap<Entities.ShipmentType, ShipmentTypes>();
            Mapper.CreateMap<ShipmentTypes, Entities.ShipmentType>();
        }
        public int Add(Entities.ShipmentType addItem)
        {
            ShipmentTypes shipmentType = Mapper.Map<ShipmentTypes>(addItem);
            try
            {
                context.ShipmentTypes.Add(shipmentType);
                context.SaveChanges();

                return shipmentType.IDshipment;
            }
            catch (SqlException ex)
            {
                return -1;
            }
        }

        public bool Delete(int deleteItem)
        {
            var shipmentType = context.ShipmentTypes.First(x => x.IDshipment == deleteItem);
            try
            {
                shipmentType.IsActive = false;
                context.Entry(shipmentType).State = EntityState.Modified;
                context.SaveChanges();

                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }
        }

        public Entities.ShipmentType Get(int itemID)
        {
            ShipmentTypes shipmentType = new ShipmentTypes();
            try
            {
                shipmentType = context.ShipmentTypes.Where(x => x.IDshipment == itemID).FirstOrDefault();
            }
            catch (SqlException ex)
            {
                return new Entities.ShipmentType();
            }
            Entities.ShipmentType newShipmentType = Mapper.Map<Entities.ShipmentType>(shipmentType);
            return newShipmentType;
        }

        public Entities.ShipmentTypes Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDshipment", string searchText = "")
        {
            var shipmentTypes = (from shipmentType in context.ShipmentTypes
                                 where shipmentType.IsActive == true
                                 select shipmentType);

            if (!string.IsNullOrEmpty(searchText))
            {
                shipmentTypes = (from types in shipmentTypes
                                 where types.ShipmentName.ToLower().Contains(searchText.ToLower()) ||
                                       types.ShipmentPrice.ToString().Contains(searchText.ToString()) ||
                                       types.Code.ToLower().Contains(searchText.ToLower())
                                 select types);
            }

            int count = shipmentTypes.Count();
            if (jtPageSize == 0)
            {
                shipmentTypes = shipmentTypes.OrderBy(jtSorting);
            }
            else
            {
                shipmentTypes = shipmentTypes.OrderBy(jtSorting).Skip(jtStartIndex).Take(jtPageSize);
            }

            Entities.ShipmentTypes mainShipmentTypes = new Entities.ShipmentTypes
            {
                TotalCount = count,
                ShipmentTypeList = Mapper.Map<List<ShipmentTypes>, List<Entities.ShipmentType>>(shipmentTypes.ToList())
            };

            return mainShipmentTypes;
        }

        public bool Update(Entities.ShipmentType updateItem)
        {
            ShipmentTypes shipmentType = Mapper.Map<ShipmentTypes>(updateItem);
            if (shipmentType != null)
            {
                try
                {
                    context.Entry(shipmentType).State = EntityState.Modified;
                    context.SaveChanges();
                }
                catch (SqlException ex)
                {
                    return false;
                }
            }
            return true;
        }

        public Entities.ShipmentTypes SearchShipmentByPayment(int paymentID)
        {
            var posibilities = (from paymentType in context.PaymentShipment
                                 where paymentType.PaymentID == paymentID
                                 select paymentType.ShipmentID).ToList();

            var shipmentTypes = (from shipmentType in context.ShipmentTypes
                                 where posibilities.Contains(shipmentType.IDshipment)
                                 select shipmentType);

            int count = shipmentTypes.Count();
            shipmentTypes = shipmentTypes.OrderBy("IDshipment");

            Entities.ShipmentTypes mainShipmentTypes = new Entities.ShipmentTypes
            {
                TotalCount = count,
                ShipmentTypeList = Mapper.Map<List<ShipmentTypes>, List<Entities.ShipmentType>>(shipmentTypes.ToList())
            };

            return mainShipmentTypes;

        }
    }
}
