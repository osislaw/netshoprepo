//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NetShop.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class Realizations
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Realizations()
        {
            this.RealizationItem = new HashSet<RealizationItem>();
        }
    
        public int IDrealization { get; set; }
        public int CustomerID { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public Nullable<decimal> TotalAmountBrutto { get; set; }
        public System.DateTime CreateDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public bool IsDeleted { get; set; }
        public string Comment { get; set; }
        public int RealizationStatusID { get; set; }
        public int RealizationAddressID { get; set; }
        public int ShipmentID { get; set; }
        public int PaymentID { get; set; }
    
        public virtual Customer Customer { get; set; }
        public virtual PaymentTypes PaymentTypes { get; set; }
        public virtual RealizationAddress RealizationAddress { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RealizationItem> RealizationItem { get; set; }
        public virtual RealizationStatus RealizationStatus { get; set; }
        public virtual ShipmentTypes ShipmentTypes { get; set; }
    }
}
