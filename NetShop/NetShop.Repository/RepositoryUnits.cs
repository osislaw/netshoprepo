﻿using NetShop.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Dynamic;
using Entities = NetShop.Contracts.Class;
using AutoMapper;
using System.Data.SqlClient;
using System.Data.Entity;

namespace NetShop.Repository
{
    public class RepositoryUnits : IRepositoryUnits
    {
        NetShopEntities context = new NetShopEntities();
        public RepositoryUnits()
        {
            Mapper.CreateMap<Entities.Unit, Units>();
            Mapper.CreateMap<Units, Entities.Unit>();
        }
        public int Add(Entities.Unit addItem)
        {
            Units unit = Mapper.Map<Units>(addItem);
            try
            {
                context.Units.Add(unit);
                context.SaveChanges();

                return unit.IDunit;
            }
            catch (SqlException ex)
            {
                return -1;
            }
        }

        public bool Delete(int deleteItem)
        {
            var unit = context.Units.First(x => x.IDunit == deleteItem);
            try
            {
                unit.IsDeleted = true;
                context.Entry(unit).State = EntityState.Modified;
                context.SaveChanges();

                return true;
            }
            catch (SqlException ex)
            {
                return false;
            }
        }

        public Entities.Unit Get(int itemID)
        {
            Units unit = new Units();
            try
            {
                unit = context.Units.Where(x => x.IDunit == itemID).FirstOrDefault();
            }
            catch (SqlException ex)
            {
                return new Entities.Unit();
            }
            Entities.Unit newUnit = Mapper.Map<Entities.Unit>(unit);
            return newUnit;
        }

        public Entities.Units Search(int jtStartIndex, int jtPageSize, string jtSorting = "IDunit", string searchText = "")
        {
            var units = (from unit in context.Units
                              where unit.IsDeleted == false
                              select unit);

            if (!string.IsNullOrEmpty(searchText))
            {
                units = (from unit in units
                              where unit.Description.ToLower().Contains(searchText.ToLower()) ||
                                    unit.Name.ToLower().Contains(searchText.ToLower())
                              select unit);
            }

            int count = units.Count();
            if (jtPageSize == 0)
            {
                units = units.OrderBy(jtSorting);
            }
            else
            {
                units = units.OrderBy(jtSorting).Skip(jtStartIndex).Take(jtPageSize);
            }

            Entities.Units mainUnits = new Entities.Units
            {
                TotalCount = count,
                UnitList = Mapper.Map<List<Units>, List<Entities.Unit>>(units.ToList())
            };

            return mainUnits;
        }

        public bool Update(Entities.Unit updateItem)
        {
            Units unit = Mapper.Map<Units>(updateItem);
            if (unit != null)
            {
                try
                {
                    context.Entry(unit).State = EntityState.Modified;
                    context.SaveChanges();
                }
                catch (SqlException ex)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
