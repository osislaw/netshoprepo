﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE UpdateRealizationValue
	-- Add the parameters for the stored procedure here
	@realizationID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE Realizations 
	set TotalAmount = (Select SUM(ProductPrice) from RealizationItem where RealizationID=@realizationID), 
	TotalAmountBrutto = (Select SUM(ProductPriceBrutto) from RealizationItem where RealizationID=@realizationID) 
	where IDrealization=@realizationID;
END