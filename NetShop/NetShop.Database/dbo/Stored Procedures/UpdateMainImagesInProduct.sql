﻿
CREATE PROCEDURE UpdateMainImagesInProduct 
	-- Add the parameters for the stored procedure here
	@productID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Update Images set IsMain=0 where ProductID=@productID
END