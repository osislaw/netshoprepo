﻿CREATE TABLE [dbo].[RealizationAddress] (
    [IDrealizationAddress] INT            IDENTITY (1, 1) NOT NULL,
    [FirstName]            NVARCHAR (50)  NULL,
    [Surname]              NVARCHAR (50)  NULL,
    [City]                 NVARCHAR (255) NULL,
    [Street]               NVARCHAR (255) NULL,
    [ZipCode]              NVARCHAR (30)  NULL,
    [CompanyName]          NVARCHAR (200) NULL,
    [CompanyNIP]           NVARCHAR (30)  NULL,
    [CompanyCity]          NVARCHAR (255) NULL,
    [CompanyStreet]        NVARCHAR (255) NULL,
    [CompanyZipCode]       NVARCHAR (255) NULL,
    [Email]                NVARCHAR (50)  NULL,
    [Phone]                NVARCHAR (20)  NULL,
    [IsDeleted]            BIT            CONSTRAINT [DF_RealizationAddress_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_RealizationAddress] PRIMARY KEY CLUSTERED ([IDrealizationAddress] ASC)
);







