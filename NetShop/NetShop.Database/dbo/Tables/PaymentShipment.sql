﻿CREATE TABLE [dbo].[PaymentShipment] (
    [IDpaymentShipment] INT IDENTITY (1, 1) NOT NULL,
    [PaymentID]         INT NOT NULL,
    [ShipmentID]        INT NOT NULL,
    CONSTRAINT [PK_PaymentShipment] PRIMARY KEY CLUSTERED ([IDpaymentShipment] ASC),
    CONSTRAINT [FK_PaymentShipment_PaymentTypes] FOREIGN KEY ([PaymentID]) REFERENCES [dbo].[PaymentTypes] ([IDpayment]),
    CONSTRAINT [FK_PaymentShipment_ShipmentTypes] FOREIGN KEY ([ShipmentID]) REFERENCES [dbo].[ShipmentTypes] ([IDshipment])
);



