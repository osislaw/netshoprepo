﻿CREATE TABLE [dbo].[Products] (
    [IDproduct]    INT            IDENTITY (1, 1) NOT NULL,
    [ProductName]  NVARCHAR (255) NOT NULL,
    [CategoryID]   INT            NOT NULL,
    [Visible]      BIT            CONSTRAINT [DF_Products_Visible] DEFAULT ((1)) NOT NULL,
    [IsDeleted]    BIT            CONSTRAINT [DF_Products_IsDeleted] DEFAULT ((0)) NOT NULL,
    [Weight]       INT            NOT NULL,
    [PriceNetto]   DECIMAL (8, 2) NOT NULL,
    [PriceBrutto]  DECIMAL (8, 2) NOT NULL,
    [ProducerCode] NVARCHAR (50)  NULL,
    [Description]  NVARCHAR (MAX) NULL,
    [UnitID]       INT            NULL,
    CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED ([IDproduct] ASC),
    CONSTRAINT [FK_Products_Categories] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[Categories] ([IDcategory]),
    CONSTRAINT [FK_Products_Units] FOREIGN KEY ([UnitID]) REFERENCES [dbo].[Units] ([IDunit])
);





