﻿CREATE TABLE [dbo].[Categories] (
    [IDcategory]   INT            IDENTITY (1, 1) NOT NULL,
    [ParentID]     INT            NOT NULL,
    [CategoryName] NVARCHAR (255) NOT NULL,
    [Description]  NVARCHAR (MAX) NULL,
    [Visible]      BIT            CONSTRAINT [DF_Categories_Visible] DEFAULT ((1)) NOT NULL,
    [IsDeleted]    BIT            CONSTRAINT [DF_Categories_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED ([IDcategory] ASC)
);



