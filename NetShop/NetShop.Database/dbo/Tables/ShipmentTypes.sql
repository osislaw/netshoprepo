﻿CREATE TABLE [dbo].[ShipmentTypes] (
    [IDshipment]    INT            IDENTITY (1, 1) NOT NULL,
    [ShipmentName]  NVARCHAR (150) NOT NULL,
    [ShipmentPrice] DECIMAL (8, 2) NOT NULL,
    [IsActive]      BIT            CONSTRAINT [DF_ShipmentTypes_IsActive] DEFAULT ((1)) NOT NULL,
    [Code]          NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_ShipmentTypes] PRIMARY KEY CLUSTERED ([IDshipment] ASC)
);









