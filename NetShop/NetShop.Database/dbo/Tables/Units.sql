﻿CREATE TABLE [dbo].[Units] (
    [IDunit]      INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50)  NOT NULL,
    [Description] NVARCHAR (150) NULL,
    [IsDeleted]   BIT            NOT NULL,
    CONSTRAINT [PK_Units] PRIMARY KEY CLUSTERED ([IDunit] ASC)
);

