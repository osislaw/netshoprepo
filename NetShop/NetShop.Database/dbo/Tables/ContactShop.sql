﻿CREATE TABLE [dbo].[ContactShop] (
    [IDcontactShop]  INT            IDENTITY (1, 1) NOT NULL,
    [Phone]          NVARCHAR (50)  NULL,
    [City]           NVARCHAR (255) NULL,
    [Street]         NVARCHAR (255) NULL,
    [ZipCode]        NVARCHAR (30)  NULL,
    [NIP]            NVARCHAR (30)  NULL,
    [OwnerFirstName] NVARCHAR (50)  NULL,
    [OwnerSurname]   NVARCHAR (50)  NULL,
    CONSTRAINT [PK_ContactShop] PRIMARY KEY CLUSTERED ([IDcontactShop] ASC)
);



