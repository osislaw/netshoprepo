﻿CREATE TABLE [dbo].[News] (
    [IDnews]           INT            IDENTITY (1, 1) NOT NULL,
    [Title]            NVARCHAR (MAX) NOT NULL,
    [ShortDescription] NVARCHAR (MAX) NOT NULL,
    [Description]      NVARCHAR (MAX) NOT NULL,
    [CreateDate]       DATETIME       NOT NULL,
    [Author]           NVARCHAR (255) NOT NULL,
    [IsDeleted]        BIT            CONSTRAINT [DF_News_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED ([IDnews] ASC)
);







