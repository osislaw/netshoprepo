﻿CREATE TABLE [dbo].[Customer] (
    [IDcustomer]     INT            IDENTITY (1, 1) NOT NULL,
    [FirstName]      NVARCHAR (50)  NOT NULL,
    [Surname]        NVARCHAR (50)  NOT NULL,
    [City]           NVARCHAR (255) NOT NULL,
    [Street]         NVARCHAR (255) NOT NULL,
    [ZipCode]        NVARCHAR (30)  NOT NULL,
    [CompanyName]    NVARCHAR (200) NULL,
    [CompanyNIP]     NVARCHAR (30)  NULL,
    [CompanyCity]    NVARCHAR (255) NULL,
    [CompanyStreet]  NVARCHAR (255) NULL,
    [CompanyZipCode] NVARCHAR (30)  NULL,
    [UserProfileID]  INT            NOT NULL,
    [Email]          NVARCHAR (50)  NOT NULL,
    [Phone]          NVARCHAR (20)  NOT NULL,
    [IsDeleted]      BIT            CONSTRAINT [DF_Customer_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([IDcustomer] ASC),
    CONSTRAINT [FK_Customer_UserProfile] FOREIGN KEY ([UserProfileID]) REFERENCES [dbo].[UserProfile] ([UserId])
);











