﻿CREATE TABLE [dbo].[RealizationItem] (
    [IDrealizationItem]       INT            IDENTITY (1, 1) NOT NULL,
    [RealizationID]           INT            NOT NULL,
    [ProductID]               INT            NOT NULL,
    [ProductPrice]            DECIMAL (8, 2) NOT NULL,
    [ProductPriceBrutto]      DECIMAL (8, 2) NOT NULL,
    [ProductName]             NVARCHAR (255) NOT NULL,
    [RealizationItemStatusID] INT            NOT NULL,
    [IsDeleted]               BIT            CONSTRAINT [DF_RealizationItem_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED ([IDrealizationItem] ASC),
    CONSTRAINT [FK_Orders_Realizations] FOREIGN KEY ([RealizationID]) REFERENCES [dbo].[Realizations] ([IDrealization]),
    CONSTRAINT [FK_Table_1_OrderStatus] FOREIGN KEY ([RealizationItemStatusID]) REFERENCES [dbo].[RealizationItemStatus] ([IDrealizationItemStatus]),
    CONSTRAINT [FK_Table_1_Products] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Products] ([IDproduct])
);



