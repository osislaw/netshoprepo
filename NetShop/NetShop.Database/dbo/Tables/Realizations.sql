﻿CREATE TABLE [dbo].[Realizations] (
    [IDrealization]        INT            IDENTITY (1, 1) NOT NULL,
    [CustomerID]           INT            NOT NULL,
    [TotalAmount]          DECIMAL (8, 2) NULL,
    [TotalAmountBrutto]    DECIMAL (8, 2) NULL,
    [CreateDate]           DATETIME       NOT NULL,
    [EndDate]              DATETIME       NULL,
    [IsDeleted]            BIT            CONSTRAINT [DF_Realizations_IsDeleted] DEFAULT ((0)) NOT NULL,
    [Comment]              NVARCHAR (MAX) NULL,
    [RealizationStatusID]  INT            NOT NULL,
    [RealizationAddressID] INT            NOT NULL,
    [ShipmentID]           INT            NOT NULL,
    [PaymentID]            INT            NOT NULL,
    CONSTRAINT [PK_Realizations] PRIMARY KEY CLUSTERED ([IDrealization] ASC),
    CONSTRAINT [FK_Realizations_Customer] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customer] ([IDcustomer]),
    CONSTRAINT [FK_Realizations_PaymentTypes] FOREIGN KEY ([PaymentID]) REFERENCES [dbo].[PaymentTypes] ([IDpayment]),
    CONSTRAINT [FK_Realizations_RealizationAddress] FOREIGN KEY ([RealizationAddressID]) REFERENCES [dbo].[RealizationAddress] ([IDrealizationAddress]),
    CONSTRAINT [FK_Realizations_RealizationStatus] FOREIGN KEY ([RealizationStatusID]) REFERENCES [dbo].[RealizationStatus] ([IDrealizationStatus]),
    CONSTRAINT [FK_Realizations_ShipmentTypes] FOREIGN KEY ([ShipmentID]) REFERENCES [dbo].[ShipmentTypes] ([IDshipment])
);





