﻿CREATE TABLE [dbo].[Images] (
    [IDimage]   INT            IDENTITY (1, 1) NOT NULL,
    [ProductID] INT            NOT NULL,
    [Path]      NVARCHAR (MAX) NOT NULL,
    [IsActive]  BIT            CONSTRAINT [DF_Images_IsActive] DEFAULT ((1)) NOT NULL,
    [IsMain]    BIT            CONSTRAINT [DF_Images_IsMain] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Images] PRIMARY KEY CLUSTERED ([IDimage] ASC),
    CONSTRAINT [FK_Images_Products] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Products] ([IDproduct])
);



