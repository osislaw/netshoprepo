﻿CREATE TABLE [dbo].[RealizationStatus] (
    [IDrealizationStatus] INT           IDENTITY (1, 1) NOT NULL,
    [StatusName]          NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_RealizationStatus] PRIMARY KEY CLUSTERED ([IDrealizationStatus] ASC)
);



