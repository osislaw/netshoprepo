﻿CREATE TABLE [dbo].[Baskets] (
    [IDbasket]     INT           IDENTITY (1, 1) NOT NULL,
    [SessionID]    NVARCHAR (50) NOT NULL,
    [ProductID]    INT           NOT NULL,
    [ProductCount] INT           NOT NULL,
    CONSTRAINT [PK_Basket] PRIMARY KEY CLUSTERED ([IDbasket] ASC),
    CONSTRAINT [FK_Basket_Basket] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Products] ([IDproduct])
);

