﻿CREATE TABLE [dbo].[PaymentTypes] (
    [IDpayment]   INT            IDENTITY (1, 1) NOT NULL,
    [PaymentName] NVARCHAR (150) NOT NULL,
    [IsActive]    BIT            CONSTRAINT [DF_PaymentTypes_IsActive] DEFAULT ((1)) NOT NULL,
    [Code]        NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_PaymentTypes] PRIMARY KEY CLUSTERED ([IDpayment] ASC)
);





