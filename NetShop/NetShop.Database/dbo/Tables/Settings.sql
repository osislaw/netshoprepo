﻿CREATE TABLE [dbo].[Settings] (
    [IDsetting]   INT            IDENTITY (1, 1) NOT NULL,
    [Code]        NVARCHAR (50)  NOT NULL,
    [Value]       NVARCHAR (MAX) NOT NULL,
    [Name]        NVARCHAR (255) NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    [ValueTypeID] INT            NOT NULL,
    [IsDeleted]   BIT            NOT NULL,
    [Creator]     NVARCHAR (1)   NOT NULL,
    CONSTRAINT [PK_Settings] PRIMARY KEY CLUSTERED ([IDsetting] ASC),
    CONSTRAINT [FK_Settings_ValueTypes] FOREIGN KEY ([ValueTypeID]) REFERENCES [dbo].[ValueTypes] ([IDvalueType])
);








GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Settings]
    ON [dbo].[Settings]([Code] ASC);

