﻿CREATE TABLE [dbo].[ValueTypes] (
    [IDvalueType] INT           NOT NULL,
    [Type]        VARCHAR (30)  NOT NULL,
    [Description] VARCHAR (MAX) NULL,
    [IsDeleted]   BIT           NOT NULL,
    [Creator]     VARCHAR (1)   NOT NULL,
    CONSTRAINT [PK_ValueTypes] PRIMARY KEY CLUSTERED ([IDvalueType] ASC)
);






GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ValueTypes]
    ON [dbo].[ValueTypes]([IDvalueType] ASC);

