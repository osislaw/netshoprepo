﻿CREATE TABLE [dbo].[UserProfile] (
    [UserId]   INT            IDENTITY (1, 1) NOT NULL,
    [UserName] NVARCHAR (150) NOT NULL,
    CONSTRAINT [PK__UserProf__1788CC4C82158835] PRIMARY KEY CLUSTERED ([UserId] ASC),
    CONSTRAINT [UQ__UserProf__C9F284561B04ABA3] UNIQUE NONCLUSTERED ([UserName] ASC)
);



