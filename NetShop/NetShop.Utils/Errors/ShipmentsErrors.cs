﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Utils.Errors
{
    public static class ShipmentsErrors
    {
        public const string SHIPMENTS_ADD_ERROR = "Błąd podczas dodawania rodzaju wysyłki!";
        public const string SHIPMENTS_UPDATE_ERROR = "Bład podczas aktualizacji rodzaju wysyłki";
        public const string SHIPMENTS_DELETE_ERROR = "Bład podczas usuwania rodzaju wysyłki";
        public const string SHIPMENTS_SEARCH_ERROR = "Bład podczas wyszukiwania rodzaju wysyłki";
    }
}
