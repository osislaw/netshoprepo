﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetShop.Utils.Errors
{
    public static class CustomersErrors
    {
        public const string CLIENT_ADD_ERROR = "Błąd dodania klienta";
        public const string CLIENT_UPDATE_ERROR = "Bład podczas aktualizacji danych klienta";
        public const string CLIENT_DELETE_ERROR = "Bład podczas usuwania danych klienta";
        public const string USERNAME_EXISTS = "Istnieje klient o podanym loginie!";
    }
}
