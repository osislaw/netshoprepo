﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Utils.Errors
{
    public static class ProductsErrors
    {
        public const string PRODUCTS_ADD_ERROR = "Błąd dodania produktu";
        public const string PRODUCTS_UPDATE_ERROR = "Bład podczas aktualizacji produktu";
        public const string PRODUCTS_DELETE_ERROR = "Bład podczas usuwania produktu";
        public const string PRODUCTS_SEARCH_ERROR = "Bład podczas wyszukiwania produktu";

        public const string IMAGES_ADD_ERROR = "Błąd dodania zdjecia produktu";
        public const string IMAGES_UPDATE_ERROR = "Bład podczas aktualizacji zdjecia produktu";
        public const string IMAGES_DELETE_ERROR = "Bład podczas usuwania zdjecia produktu";
        public const string IMAGES_SEARCH_ERROR = "Bład podczas wyszukiwania zdjec produktu";
    }
}
