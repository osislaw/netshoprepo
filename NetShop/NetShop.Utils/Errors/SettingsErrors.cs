﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Utils.Errors
{
    public static class SettingsErrors
    {
        public const string SETTINGS_ADD_ERROR = "Błąd podczas dodawania ustawienia globalnego!";
        public const string SETTINGS_UPDATE_ERROR = "Bład podczas aktualizacji ustawienia globalnego";
        public const string SETTINGS_DELETE_ERROR = "Bład podczas usuwania ustawienia globalnego";
        public const string SETTINGS_SEARCH_ERROR = "Bład podczas wyszukiwania ustawienia globalnego";
    }
}
