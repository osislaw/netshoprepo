﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Utils.Errors
{
    public static class PaymentsErrors
    {
        public const string PAYMENTS_ADD_ERROR = "Błąd podczas dodawania typu płatności!";
        public const string PAYMENTS_UPDATE_ERROR = "Bład podczas aktualizacji typu płatności";
        public const string PAYMENTS_DELETE_ERROR = "Bład podczas usuwania typu płatności";
        public const string PAYMENTS_SEARCH_ERROR = "Bład podczas wyszukiwania typu płatności";
    }
}
