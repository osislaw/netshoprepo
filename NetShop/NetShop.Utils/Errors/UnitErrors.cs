﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Utils.Errors
{
    public static class UnitErrors
    {
        public const string UNIT_ADD_ERROR = "Błąd podczas dodawania rodzaju jednostki!";
        public const string UNIT_UPDATE_ERROR = "Bład podczas aktualizacji rodzaju jednostki";
        public const string UNIT_DELETE_ERROR = "Bład podczas usuwania rodzaju jednostki";
        public const string UNIT_SEARCH_ERROR = "Bład podczas wyszukiwania rodzaju jednostki";
    }
}
