﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Utils.Errors
{
    public class PaymentShipmentErrors
    {
        public const string PAYMENTSSHIPMENTS_ADD_ERROR = "Błąd podczas dodawania powiązania!";
        public const string PAYMENTSSHIPMENTS_UPDATE_ERROR = "Bład podczas aktualizacji powiązania";
        public const string PAYMENTSSHIPMENTS_DELETE_ERROR = "Bład podczas usuwania powiązania";
        public const string PAYMENTSSHIPMENTS_SEARCH_ERROR = "Bład podczas wyszukiwania powiązania";
        public const string PAYMENTSSHIPMENTS_EXISTS_ERROR = "Powiązanie już istnieje!";
    
    }
}
