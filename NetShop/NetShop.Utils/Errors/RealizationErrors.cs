﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Utils.Errors
{
    public static class RealizationErrors
    {
        public const string ERRORPAYMENTANDSHIPMENT = "Błąd ustawienia płatnoci lub wysyłki";
        public const string ERRORINUPDATE = "Błąd podczas aktualizacji zamówienia";
        public const string ERRORINUPDATEITEM = "Błąd podczas aktualizacji produktu w zamówieniu";
        public const string ERRORINDELETE = "Błąd podczas usuwania zamówienia";
        public const string ERRORINDELETEITEM = "Błąd podczas usuwania produktu z zamówienia";
        public const string ERRORINADDITEM = "Błąd dodania produktu do zamówienia";
    }
}
