﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Utils.Errors
{
    public static class NewsErrors
    {
        public const string NEWS_ADD_ERROR = "Błąd dodania newsu";
        public const string NEWS_UPDATE_ERROR = "Bład podczas aktualizacji newsu";
        public const string NEWS_DELETE_ERROR = "Bład podczas usuwania newsu";
    }
}
