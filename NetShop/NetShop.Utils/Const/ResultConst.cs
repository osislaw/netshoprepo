﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetShop.Utils.Const
{
    public static class ResultConst
    {
        public const string OK = "OK";
        public const string ERROR = "ERROR";
        public const string resultOK = "Zapisano poprawnie dane.";
        public const string resultERROR = "Wystąpił błąd podczas zapisywania danych.";
    }
}
