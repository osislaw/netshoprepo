﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetShop.Utils.Const
{
    public static class ValueTypesConst
    {
        public const string INT = "INT";
        public const string DOUBLE = "DOUBLE";
        public const string FLOAT = "FLOAT";
        public const string STRING = "STRING";
        public const string BOOL = "BOOL";
    }
}
