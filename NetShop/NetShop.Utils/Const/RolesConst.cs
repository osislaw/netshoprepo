﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetShop.Utils.Const
{
    public static class RolesConst
    {
        public const string Admin = "Admin";
        public const string Customer = "Customer";
    }
}
