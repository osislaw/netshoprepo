﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class SearchItemController : Controller
    {
        //
        // GET: /CmsSearchItem/

        public ActionResult SearchItem(string searchText, int pageNumber, string priceSort, string nameSort, int categoryID)
        {
            if(categoryID > 0)
            {
                RedirectToAction("CategoryItem", "CategoryItem", new { categoryID = categoryID });
            }
            IRepositoryCategories repository = new NetShop.Repository.RepositoryCategories();
            List<TreeCategory> categories = repository.GetTree();
            string sortBy = string.Empty;
            if(priceSort == "DESC")
            {
                sortBy = "PriceBrutto DESC";
            }
            else
            {
                sortBy = "PriceBrutto";
            }

            if (nameSort == "DESC")
            {
                sortBy += ", ProductName DESC";
            }
            else
            {
                sortBy += ", ProductName";
            }

            if (pageNumber < 1)
            {
                pageNumber = 1;
            }

            IRepositoryProducts repositoryProducts = new NetShop.Repository.RepositoryProducts();
            Products products = repositoryProducts.Search(pageNumber-1, 20, sortBy, searchText);
            foreach (Product product in products.ProductList)
            {
                product.ProductImage = repositoryProducts.GetMainImage(product.IDproduct);
            }

            CategoryViewModel mainModel = new CategoryViewModel();
            int lastIndex = -1;
            if (products.TotalCount % 20 == 0)
            {
                lastIndex = products.TotalCount / 20;
            }
            else
            {
                lastIndex = products.TotalCount / 20 + 1;
            }
            mainModel.PageNumber = pageNumber;
            mainModel.LastIndex = lastIndex;
            mainModel.TreeCategories = categories;
            mainModel.ProductList = products.ProductList;
            mainModel.SearchText = searchText;
            return View(mainModel);
        }
    }
}
