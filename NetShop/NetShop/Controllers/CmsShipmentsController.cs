﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Utils.Const;
using NetShop.Utils.Errors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class CmsShipmentsController : Controller
    {
        //
        // GET: /CmsShipments/

        public ActionResult Shipments()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Shipments(int jtStartIndex = 0, int jtPageSize = 20, string jtSorting = null, string searchText = null)
        {
            try
            {
                IRepositoryShipmentTypes repository = new NetShop.Repository.RepositoryShipmentTypes();
                var shipmentTypes = repository.Search(jtStartIndex, jtPageSize, string.IsNullOrEmpty(jtSorting) ? "IDshipment DESC" : jtSorting, searchText);
                var shipmentsAddList = repository.Search(jtStartIndex = 0, jtPageSize = 0, string.IsNullOrEmpty(jtSorting) ? "ShipmentName" : jtSorting, searchText: searchText).ShipmentTypeList.Select(c => new { DisplayText = c.ShipmentName, Value = c.IDshipment });
                var newShipmentsList = new List<object> { new { DisplayText = "", Value = -1 } };
                newShipmentsList.AddRange(shipmentsAddList.ToList().OrderBy(x => x.DisplayText));
                return Json(new { Result = ResultConst.OK, Records = shipmentTypes.ShipmentTypeList, TotalRecordCount = shipmentTypes.TotalCount, Options = newShipmentsList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult CreateShipment(ShipmentType shipmentType)
        {
            try
            {
                IRepositoryShipmentTypes repository = new NetShop.Repository.RepositoryShipmentTypes();
                int id = repository.Add(shipmentType);
                if (id > 0)
                {
                    shipmentType = repository.Get(id);
                    return Json(new { Result = ResultConst.OK, Record = shipmentType }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = ShipmentsErrors.SHIPMENTS_ADD_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteShipment(int IDshipment)
        {
            try
            {
                IRepositoryShipmentTypes repository = new NetShop.Repository.RepositoryShipmentTypes();

                bool isOk = repository.Delete(IDshipment);
                if (isOk)
                {
                    return Json(new { Result = ResultConst.OK }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = ShipmentsErrors.SHIPMENTS_DELETE_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult UpdateShipment(ShipmentType shipmentType)
        {
            try
            {
                IRepositoryShipmentTypes repository = new NetShop.Repository.RepositoryShipmentTypes();
                bool isOK = repository.Update(shipmentType);
                if (isOK)
                {
                    shipmentType = repository.Get(shipmentType.IDshipment);
                    return Json(new { Result = ResultConst.OK, Record = shipmentType }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = ShipmentsErrors.SHIPMENTS_UPDATE_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
