﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class OrderController : Controller
    {
        //
        // GET: /Order/

        public ActionResult Order(int realizationID)
        {
            OrderViewModel model = new OrderViewModel();
            IRepositoryCategories repositoryCategories = new NetShop.Repository.RepositoryCategories();
            List<TreeCategory> categories = repositoryCategories.GetTree();
            model.TreeCategories = categories;

            IRepositoryRealizations repository = new NetShop.Repository.RepositoryRealizations();
            model.Realization = repository.Get(realizationID);
            model.RealizationItems = repository.GetItems(realizationID);
            model.CustomerID = model.Realization.CustomerID;

            IRepositoryProducts repositoryProducts = new NetShop.Repository.RepositoryProducts();
            foreach (Order item in model.RealizationItems)
            {
                item.ProductImage = repositoryProducts.GetMainImage(item.ProductID);
            }

            switch(model.Realization.RealizationStatusID)
            {
                case 1:
                    model.Realization.StatusName = "Nowe";
                    break;
                case 2:
                    model.Realization.StatusName = "Do zapłaty";
                    break;
                case 3:
                    model.Realization.StatusName = "Do wysłania";
                    break;
                case 4:
                    model.Realization.StatusName = "Wysłane";
                    break;
                case 5:
                    model.Realization.StatusName = "Zakończone";
                    break;
            }

            return View(model);
        }

    }
}
