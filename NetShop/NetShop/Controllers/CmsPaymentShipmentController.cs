﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Utils.Const;
using NetShop.Utils.Errors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class CmsPaymentShipmentController : Controller
    {
        //
        // GET: /CmsPaymentShipment/

        public ActionResult PaymentShipment()
        {
            return View();
        }

        [HttpPost]
        public JsonResult PaymentShipment(int jtStartIndex = 0, int jtPageSize = 20, string jtSorting = null, string searchText = null)
        {
            try
            {
                IRepositoryPaymentShipment repository = new NetShop.Repository.RepositoryPaymentShipment();
                var paymentsshipments = repository.Search(jtStartIndex, jtPageSize, string.IsNullOrEmpty(jtSorting) ? "IDpaymentShipment DESC" : jtSorting, searchText);
                return Json(new { Result = ResultConst.OK, Records = paymentsshipments.PaymentShipmentList, TotalRecordCount = paymentsshipments.TotalCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CreatePaymentShipment(PaymentShipment ps)
        {
            try
            {
                IRepositoryPaymentShipment repository = new NetShop.Repository.RepositoryPaymentShipment();
                if (ps.PaymentID > 0 && ps.ShipmentID > 0)
                {
                    if (!repository.PaymentShipmentExists(ps.PaymentID, ps.ShipmentID))
                    {
                        int id = repository.Add(ps);
                        if (id > 0)
                        {
                            ps = repository.Get(id);
                            return Json(new { Result = ResultConst.OK, Record = ps }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { Result = ResultConst.ERROR, Message = PaymentShipmentErrors.PAYMENTSSHIPMENTS_ADD_ERROR }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { Result = ResultConst.ERROR, Message = PaymentShipmentErrors.PAYMENTSSHIPMENTS_EXISTS_ERROR }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = PaymentShipmentErrors.PAYMENTSSHIPMENTS_ADD_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeletePaymentShipment(int IDpaymentShipment)
        {
            try
            {
                IRepositoryPaymentShipment repository = new NetShop.Repository.RepositoryPaymentShipment();
                bool isOk = repository.Delete(IDpaymentShipment);
                if (isOk)
                {
                    return Json(new { Result = ResultConst.OK }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = PaymentShipmentErrors.PAYMENTSSHIPMENTS_DELETE_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdatePaymentShipment(PaymentShipment ps)
        {
            try
            {
                IRepositoryPaymentShipment repository = new NetShop.Repository.RepositoryPaymentShipment();
                if (ps.PaymentID > 0 && ps.ShipmentID > 0)
                {
                    if (!repository.PaymentShipmentExists(ps.PaymentID, ps.ShipmentID))
                    {
                        bool isOK = repository.Update(ps);
                        if (isOK)
                        {
                            ps = repository.Get(ps.IDpaymentShipment);
                            return Json(new { Result = ResultConst.OK, Record = ps }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { Result = ResultConst.ERROR, Message = UnitErrors.UNIT_UPDATE_ERROR }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { Result = ResultConst.ERROR, Message = PaymentShipmentErrors.PAYMENTSSHIPMENTS_EXISTS_ERROR }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = PaymentShipmentErrors.PAYMENTSSHIPMENTS_ADD_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
