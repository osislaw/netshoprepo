﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class ProductController : Controller
    {
        //
        // GET: /Product/

        public ActionResult Product(int productID)
        {
            IRepositoryProducts repository = new NetShop.Repository.RepositoryProducts();
            Product product = repository.Get(productID);
            List<Image> images = repository.GetProductImages(productID).ImageList;
            product.ProductImage = images.Where(x => x.IsMain == true).FirstOrDefault();

            IRepositoryCategories repositoryCategories = new NetShop.Repository.RepositoryCategories();
            List<TreeCategory> categories = repositoryCategories.GetTree();

            Products products = repository.Search(0, 9, "Weight");
            foreach (Product productItem in products.ProductList)
            {
                productItem.ProductImage = repository.GetMainImage(productItem.IDproduct);
            }

            ProductViewModel viewModel = new ProductViewModel();
            viewModel.ProductItem = product;
            viewModel.Products = products.ProductList;
            viewModel.ImageList = images.Where(x => x.IsMain == false).ToList();
            viewModel.TreeCategories = categories;

            return View(viewModel);
        }

    }
}
