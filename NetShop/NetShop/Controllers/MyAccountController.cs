﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Models;
using NetShop.Utils.Const;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace NetShop.Controllers
{
    public class MyAccountController : Controller
    {
        //
        // GET: /MyAccount/

        public ActionResult MyAccount(string result)
        {
            MyAccountViewModel model = new MyAccountViewModel();
            IRepositoryCustomers repository = new NetShop.Repository.RepositoryCustomers();
            Customer customer = repository.Get(Membership.GetUser(true).UserName.ToString());
            model.Customer = customer;

            IRepositoryCategories repositoryCategories = new NetShop.Repository.RepositoryCategories();
            List<TreeCategory> categories = repositoryCategories.GetTree();

            model.TreeCategories = categories;
            if (result == ResultConst.OK)
            {
                model.Error = false;
                model.Result = ResultConst.resultOK;
            }
            else if (result == ResultConst.ERROR)
            {
                model.Error = true;
                model.Result = ResultConst.resultERROR;
            }
            return View(model);
        }

        public ActionResult SaveCustomer(MyAccountViewModel model, int customerID, int userProfileID)
        {
            IRepositoryCustomers repository = new NetShop.Repository.RepositoryCustomers();
            Customer customer = new Customer();
            customer.City = model.Customer.City;
            customer.CompanyCity = model.Customer.CompanyCity;
            customer.CompanyName = model.Customer.CompanyName;
            customer.CompanyNIP = model.Customer.CompanyNIP;
            customer.CompanyStreet = model.Customer.CompanyStreet;
            customer.CompanyZipCode = model.Customer.CompanyZipCode;
            customer.Email = model.Customer.Email;
            customer.FirstName = model.Customer.FirstName;
            customer.IDcustomer = customerID;
            customer.Phone = model.Customer.Phone;
            customer.Street = model.Customer.Street;
            customer.Surname = model.Customer.Surname;
            customer.ZipCode = model.Customer.ZipCode;
            customer.UserProfileID = userProfileID;

            try
            {
                bool isOK = repository.Update(customer);
                if (isOK)
                {
                    return RedirectToAction("MyAccount", new { result = ResultConst.OK });
                }
                else
                {
                    return RedirectToAction("MyAccount", new { result = ResultConst.ERROR });
                }
            }
            catch(Exception ex)
            {
                return RedirectToAction("MyAccount", new { result = ResultConst.ERROR });
            }            
        }
    }
}
