﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Utils.Const;
using NetShop.Utils.Errors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class CmsNewsController : Controller
    {
        //
        // GET: /CmsNews/

        public ActionResult News()
        {
            return View();
        }

        [HttpPost]
        public JsonResult News(int jtStartIndex = 0, int jtPageSize = 20, string jtSorting = null, string searchText = null)
        {
            try
            {
                IRepositoryNews repository = new NetShop.Repository.RepositoryNews();
                var news = repository.Search(jtStartIndex, jtPageSize, string.IsNullOrEmpty(jtSorting) ? "IDnews DESC" : jtSorting, searchText);
                return Json(new { Result = ResultConst.OK, Records = news.NewsItemList, TotalRecordCount = news.TotalCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CreateNews(NewsItem news)
        {
            try
            {
                IRepositoryNews repository = new NetShop.Repository.RepositoryNews();
                int id = repository.Add(news);
                if (id > 0)
                {
                    news = repository.Get(id);
                    return Json(new { Result = ResultConst.OK, Record = news }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = NewsErrors.NEWS_ADD_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteNews(int IDnews)
        {
            try
            {
                IRepositoryNews repository = new NetShop.Repository.RepositoryNews();
                bool isOk = repository.Delete(IDnews);
                if (isOk)
                {
                    return Json(new { Result = ResultConst.OK }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = NewsErrors.NEWS_DELETE_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateNews(NewsItem news)
        {
            try
            {
                IRepositoryNews repository = new NetShop.Repository.RepositoryNews();
                bool isOK = repository.Update(news);
                if (isOK)
                {
                    news = repository.Get(news.IDnews);
                    return Json(new { Result = ResultConst.OK, Record = news }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = NewsErrors.NEWS_UPDATE_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
