﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Utils.Const;
using NetShop.Utils.Errors;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class CmsProductsController : Controller
    {
        //
        // GET: /CmsProducts/

        public ActionResult Products()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Products(int jtStartIndex = 0, int jtPageSize = 20, string jtSorting = null, string searchText = null)
        {
            try
            {
                IRepositoryProducts repository = new NetShop.Repository.RepositoryProducts();
                var products = repository.Search(jtStartIndex, jtPageSize, string.IsNullOrEmpty(jtSorting) ? "IDproduct DESC" : jtSorting, searchText);
                var productList = repository.Search(0, 0).ProductList.Select(c => new { DisplayText = c.ProductName, Value = c.IDproduct.ToString() });
                return Json(new { Result = ResultConst.OK, Records = products.ProductList, TotalRecordCount = products.TotalCount, Options = productList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ProductsErrors.PRODUCTS_SEARCH_ERROR }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CreateProduct(Product product)
        {
            try
            {
                IRepositoryProducts repository = new NetShop.Repository.RepositoryProducts();
                int id = repository.Add(product);
                if (id > 0)
                {
                    product = repository.Get(id);
                    return Json(new { Result = ResultConst.OK, Record = product }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = ProductsErrors.PRODUCTS_ADD_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteProduct(int IDproduct)
        {
            try
            {
                IRepositoryProducts repository = new NetShop.Repository.RepositoryProducts();
                bool isOk = repository.Delete(IDproduct);
                if (isOk)
                {
                    return Json(new { Result = ResultConst.OK }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = ProductsErrors.PRODUCTS_DELETE_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateProduct(Product product)
        {
            try
            {
                IRepositoryProducts repository = new NetShop.Repository.RepositoryProducts();
                bool isOK = repository.Update(product);
                if (isOK)
                {
                    product = repository.Get(product.IDproduct);
                    return Json(new { Result = ResultConst.OK, Record = product }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = ProductsErrors.PRODUCTS_UPDATE_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetProductImages(int productID)
        {
            try
            {
                IRepositoryProducts repository = new NetShop.Repository.RepositoryProducts();
                var productImages = repository.GetProductImages(productID);
                return Json(new { Result = ResultConst.OK, Records = productImages.ImageList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ProductsErrors.IMAGES_SEARCH_ERROR }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CreateProductImage(int productID, string isActive, string isMain)
        {
            var request = Request;
            var fileName = string.Empty;
            var SavefilePath = string.Empty;
            var filePath = string.Empty;

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength > 0)
                {
                    fileName = productID + "_" + Path.GetFileName(hpf.FileName);
                    SavefilePath = Path.Combine(Server.MapPath("~/Images/Products"), fileName);
                    filePath = string.Format("/Images/Products/{0}", fileName);
                    hpf.SaveAs(SavefilePath);
                }
            }

            try
            {
                Image image = new Image();
                image.Path = filePath;
                image.ProductID = productID;
                image.IsMain = isMain == "undefined" ? false : Convert.ToBoolean(isMain);
                image.IsActive = isActive == "undefined" ? false : Convert.ToBoolean(isActive);

                IRepositoryProducts repository = new NetShop.Repository.RepositoryProducts();
                int imageId = repository.AddImages(image);

                Image newImage = repository.GetProductImages(productID).ImageList.Where(x => x.IDimage == imageId).FirstOrDefault();
                return Json(new { Result = ResultConst.OK, Record = newImage }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ProductsErrors.IMAGES_ADD_ERROR }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteImage(int IDimages)
        {
            try
            {
                IRepositoryProducts repository = new NetShop.Repository.RepositoryProducts();
                repository.DeleteImage(IDimages);
                return Json(new { Result = ResultConst.OK }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ProductsErrors.IMAGES_DELETE_ERROR }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateImage(Image image)
        {
            try
            {
                IRepositoryProducts repository = new NetShop.Repository.RepositoryProducts();
                repository.UpdateImages(image);
                Images images = repository.GetProductImages(image.IDimage);
                return Json(new { Result = ResultConst.OK, Records = images.ImageList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ProductsErrors.IMAGES_UPDATE_ERROR }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
