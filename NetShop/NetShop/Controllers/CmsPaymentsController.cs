﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Utils.Const;
using NetShop.Utils.Errors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class CmsPaymentsController : Controller
    {
        //
        // GET: /CmsPayments/

        public ActionResult Payments()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Payments(int jtStartIndex = 0, int jtPageSize = 20, string jtSorting = null, string searchText = null)
        {
            try
            {
                IRepositoryPaymentTypes repository = new NetShop.Repository.RepositoryPaymentTypes();
                var paymentTypes = repository.Search(jtStartIndex, jtPageSize, string.IsNullOrEmpty(jtSorting) ? "IDpayment DESC" : jtSorting, searchText);
                var paymentsAddList = repository.Search(jtStartIndex = 0, jtPageSize = 0, string.IsNullOrEmpty(jtSorting) ? "PaymentName" : jtSorting, searchText: searchText).PaymentTypeList.Select(c => new { DisplayText = c.PaymentName, Value = c.IDpayment });
                var newPaymentsList = new List<object> { new { DisplayText = "", Value = -1 } };
                newPaymentsList.AddRange(paymentsAddList.ToList().OrderBy(x => x.DisplayText));
                return Json(new { Result = ResultConst.OK, Records = paymentTypes.PaymentTypeList, TotalRecordCount = paymentTypes.TotalCount, Options = newPaymentsList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult CreatePayment(PaymentType paymentType)
        {
            try
            {
                IRepositoryPaymentTypes repository = new NetShop.Repository.RepositoryPaymentTypes();
                int id = repository.Add(paymentType);
                if (id > 0)
                {
                    paymentType = repository.Get(id);
                    return Json(new { Result = ResultConst.OK, Record = paymentType }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = PaymentsErrors.PAYMENTS_ADD_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeletePayment(int IDpayment)
        {
            try
            {
                IRepositoryPaymentTypes repository = new NetShop.Repository.RepositoryPaymentTypes();

                bool isOk = repository.Delete(IDpayment);
                if (isOk)
                {
                    return Json(new { Result = ResultConst.OK }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = PaymentsErrors.PAYMENTS_DELETE_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult UpdatePayment(PaymentType paymentType)
        {
            try
            {
                IRepositoryPaymentTypes repository = new NetShop.Repository.RepositoryPaymentTypes();
                bool isOK = repository.Update(paymentType);
                if (isOK)
                {
                    paymentType = repository.Get(paymentType.IDpayment);
                    return Json(new { Result = ResultConst.OK, Record = paymentType }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = PaymentsErrors.PAYMENTS_UPDATE_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
