﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Models;
using NetShop.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace NetShop.Controllers
{
    public class DetailsController : Controller
    {
        //
        // GET: /Details/

        public ActionResult Details()
        {
            DetailsViewModel model = new DetailsViewModel();
            IRepositoryCustomers repository = new NetShop.Repository.RepositoryCustomers();
            if (User.Identity.IsAuthenticated)
            {
                Customer customer = repository.Get(Membership.GetUser(true).UserName.ToString());
                model.Customer = customer;
            }
            IRepositoryCategories repositoryCategories = new NetShop.Repository.RepositoryCategories();
            List<TreeCategory> categories = repositoryCategories.GetTree();

            IRepositoryPaymentTypes repositoryPaymentTypes = new NetShop.Repository.RepositoryPaymentTypes();
            PaymentTypes paymentTypes = repositoryPaymentTypes.Search(0, 0);

            IRepositoryShipmentTypes repositoryShipmentTypes = new NetShop.Repository.RepositoryShipmentTypes();
            ShipmentTypes shipmentTypes = repositoryShipmentTypes.SearchShipmentByPayment(paymentTypes.PaymentTypeList.FirstOrDefault().IDpayment);

            IRepositoryRealizations repositoryRealization = new NetShop.Repository.RepositoryRealizations();

            model.TotalPriceBrutto = repositoryRealization.GetBasket(System.Web.HttpContext.Current.Session.SessionID).Sum(x=> x.PriceBrutto * x.ProductCount);
            model.TreeCategories = categories;
            model.PaymentTypes = paymentTypes.PaymentTypeList;
            model.ShipmentTypes = shipmentTypes.ShipmentTypeList;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DetailsStep(DetailsViewModel model)
        {
            Realization realization = new Realization();
            Customer customer = new Customer();
            IRepositoryCustomers repository = new NetShop.Repository.RepositoryCustomers();
            if (User.Identity.IsAuthenticated)
            {
                customer = repository.Get(Membership.GetUser(true).UserName.ToString());
                realization.CustomerID = customer.IDcustomer;
            }
            else
            {
                customer = model.Customer;
                customer.Password = "haslo";
                customer.UserName = "@NetShop@" + Guid.NewGuid() + customer.Email;
                customer.IDcustomer = CustomersServices.CreateCustomer(customer);
                realization.CustomerID = customer.IDcustomer;
            }

            realization.RealizationStatusID = (int)Status.New;
            realization.ShipmentID = model.ShipmentID;
            realization.PaymentID = model.PaymentID;
            realization.Comment = model.Comment;
            realization.CreateDate = DateTime.Now;
            realization.EndDate = null;
            realization.TotalAmount = null;
            realization.TotalAmountBrutto = null;
            realization.IsDeleted = false;

            IRepositoryRealizations repositoryRealizations = new NetShop.Repository.RepositoryRealizations();
            RealizationAddress address = new RealizationAddress();
            address.City = model.Customer.City;
            address.CompanyCity = model.Customer.CompanyCity;
            address.CompanyName = model.Customer.CompanyName;
            address.CompanyNIP = model.Customer.CompanyNIP;
            address.CompanyStreet = model.Customer.CompanyStreet;
            address.CompanyZipCode = model.Customer.CompanyZipCode;
            address.FirstName = model.Customer.FirstName;
            address.Street = model.Customer.Street;
            address.Surname = model.Customer.Surname;
            address.ZipCode = model.Customer.ZipCode;
            address.Email = model.Customer.Email;
            address.Phone = model.Customer.Phone;

            int IDrealization = repositoryRealizations.AddRealization(realization, address, System.Web.HttpContext.Current.Session.SessionID);

            if (IDrealization > 0)
            {
                repositoryRealizations.RemoveItemsFromBasket(System.Web.HttpContext.Current.Session.SessionID);
                return RedirectToAction("Result", "Result", new { realizationID = IDrealization });
            }
            else
            {
                return RedirectToAction("Details");
            }
        }

        [HttpPost]
        public ActionResult GetShipmentTypes(int paymentID)
        {
            IRepositoryShipmentTypes repository = new NetShop.Repository.RepositoryShipmentTypes();
            ShipmentTypes shipmentTypes = repository.SearchShipmentByPayment(paymentID);

            return Json(new { types = shipmentTypes.ShipmentTypeList });
        }
    }
}
