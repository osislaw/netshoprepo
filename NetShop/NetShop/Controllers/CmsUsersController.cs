﻿using NetShop.Contracts;
using Entities = NetShop.Contracts.Class;
using NetShop.Repository;
using NetShop.Utils.Const;
using NetShop.Utils.Errors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace NetShop.Controllers
{
    public class CmsUsersController : Controller
    {
        //
        // GET: /CmsUsers/

        public ActionResult Users()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetUsers(int jtStartIndex = 0, int jtPageSize = 20, string jtSorting = null, string searchText = null)
        {
            try
            {
                IRepositoryUserProfiles repository = new RepositoryUserProfiles();
                var users = repository.GetUsers(jtStartIndex, jtPageSize, string.IsNullOrEmpty(jtSorting) ? "UserName" : jtSorting, searchText);
                return Json(new { Result = ResultConst.OK, Records = users, TotalRecordCount = users.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AddUser(Entities.UserProfile user)
        {
            try
            {
                if (!WebSecurity.UserExists(user.UserName))
                {
                    WebSecurity.CreateUserAndAccount(user.UserName, user.Password);
                    Roles.AddUserToRole(user.UserName, RolesConst.Admin);
                    return Json(new { Result = ResultConst.OK, Record = user }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = CustomersErrors.USERNAME_EXISTS }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteUser(string UserName)
        {
            try
            {
                ((SimpleMembershipProvider)Membership.Provider).DeleteAccount(UserName);
                Roles.RemoveUserFromRole(UserName, RolesConst.Admin);
                ((SimpleMembershipProvider)Membership.Provider).DeleteUser(UserName, true);
                return Json(new { Result = ResultConst.OK }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
