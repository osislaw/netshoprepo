﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Utils.Const;
using NetShop.Utils.Errors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class CmsSettingsController : Controller
    {
        //
        // GET: /CmsSettings/

        public ActionResult Settings()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Settings(int jtStartIndex = 0, int jtPageSize = 20, string jtSorting = null, string searchText = null)
        {
            try
            {
                IRepositorySettings repository = new NetShop.Repository.RepositorySettings();
                var settings = repository.Search(jtStartIndex, jtPageSize, string.IsNullOrEmpty(jtSorting) ? "IDsetting DESC" : jtSorting, searchText);
                return Json(new { Result = ResultConst.OK, Records = settings.SettingsList, TotalRecordCount = settings.TotalCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CreateSetting(Setting setting)
        {
            try
            {
                IRepositorySettings repository = new NetShop.Repository.RepositorySettings();
                int id = repository.Add(setting);
                if (id > 0)
                {
                    setting = repository.Get(id);
                    return Json(new { Result = ResultConst.OK, Record = setting }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = SettingsErrors.SETTINGS_ADD_ERROR}, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteSetting(int IDsetting)
        {
            try
            {
                IRepositorySettings repository = new NetShop.Repository.RepositorySettings();

                bool isOk = repository.Delete(IDsetting);
                if (isOk)
                {
                    return Json(new { Result = ResultConst.OK }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = SettingsErrors.SETTINGS_DELETE_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateSetting(Setting setting)
        {
            try
            {
                IRepositorySettings repository = new NetShop.Repository.RepositorySettings();
                bool isOK = repository.Update(setting);
                if (isOK)
                {
                    setting = repository.Get(setting.IDsetting);
                    Repository.GlobalSettings.Instance.Reload(setting.Code);
                    return Json(new { Result = ResultConst.OK, Record = setting }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = SettingsErrors.SETTINGS_UPDATE_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
