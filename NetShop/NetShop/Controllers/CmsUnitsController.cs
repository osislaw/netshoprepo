﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Utils.Const;
using NetShop.Utils.Errors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class CmsUnitsController : Controller
    {
        //
        // GET: /CmsUnits/

        public ActionResult Units()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Units(int jtStartIndex = 0, int jtPageSize = 20, string jtSorting = null, string searchText = null)
        {
            try
            {
                IRepositoryUnits repository = new NetShop.Repository.RepositoryUnits();
                var units = repository.Search(jtStartIndex, jtPageSize, string.IsNullOrEmpty(jtSorting) ? "IDunit DESC" : jtSorting, searchText);
                var unitAddList = repository.Search(jtStartIndex = 0, jtPageSize = 0, string.IsNullOrEmpty(jtSorting) ? "Name" : jtSorting, searchText: searchText).UnitList.Select(c => new { DisplayText = c.Name, Value = c.IDunit });
                var newUnitList = new List<object> { new { DisplayText = String.Empty, Value = String.Empty} };
                newUnitList.AddRange(unitAddList.ToList().OrderBy(x => x.DisplayText));
                return Json(new { Result = ResultConst.OK, Records = units.UnitList, TotalRecordCount = units.TotalCount, Options = newUnitList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CreateUnit(Unit unit)
        {
            try
            {
                IRepositoryUnits repository = new NetShop.Repository.RepositoryUnits();
                int id = repository.Add(unit);
                if (id > 0)
                {
                    unit = repository.Get(id);
                    return Json(new { Result = ResultConst.OK, Record = unit }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = UnitErrors.UNIT_ADD_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteUnit(int IDunit)
        {
            try
            {
                IRepositoryUnits repository = new NetShop.Repository.RepositoryUnits();

                bool isOk = repository.Delete(IDunit);
                if (isOk)
                {
                    return Json(new { Result = ResultConst.OK }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = UnitErrors.UNIT_DELETE_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateUnit(Unit unit)
        {
            try
            {
                IRepositoryUnits repository = new NetShop.Repository.RepositoryUnits();

                bool isOK = repository.Update(unit);
                if (isOK)
                {
                    unit = repository.Get(unit.IDunit);
                    return Json(new { Result = ResultConst.OK, Record = unit }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = UnitErrors.UNIT_UPDATE_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
