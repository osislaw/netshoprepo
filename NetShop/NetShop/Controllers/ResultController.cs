﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class ResultController : Controller
    {
        //
        // GET: /Result/

        public ActionResult Result(int realizationID)
        {
            ResultViewModel model = new ResultViewModel();
            IRepositoryRealizations repository = new NetShop.Repository.RepositoryRealizations();
            RealizationResult real = repository.GetRealizationResult(realizationID);
            model.Payment = real.Payment;
            model.RealizationID = real.RealizationID;
            model.Shipment = real.Shipment;
            model.TotalAmountBrutto = real.TotalAmountBrutto;
            return View(model);
        }

    }
}
