﻿using NetShop.Contracts;
using NetShop.Models;
using NetShop.Services;
using NetShop.Utils.Const;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace NetShop.Controllers
{
    public class CmsIndexController : Controller
    {
        //
        // GET: /CmsIndex/
        //[Authorize(Roles = RolesConst.Admin)]
        public ActionResult CmsIndex()
        {
            var z = Roles.GetRolesForUser(User.Identity.Name);

            if (!User.Identity.IsAuthenticated || !User.IsInRole(RolesConst.Admin))
            {
                return View("Error");
            }
            else
            {
                if(NetShop.Repository.GlobalSettings.Instance.FirstRun)
                {
                    return RedirectToAction("Settings", "CmsSettings");
                }
                return View();
            }
        }
    }
}
