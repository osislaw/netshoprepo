﻿using NetShop.Contracts;
using Entities = NetShop.Contracts.Class;
using NetShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class BasketController : Controller
    {
        //
        // GET: /Basket/

        public ActionResult Basket()
        {
            IRepositoryCategories repository = new NetShop.Repository.RepositoryCategories();
            List<Entities.TreeCategory> categories = repository.GetTree();

            IRepositoryRealizations repositoryRealization = new NetShop.Repository.RepositoryRealizations();
            List<Entities.Basket> basket = repositoryRealization.GetBasket(System.Web.HttpContext.Current.Session.SessionID);

            IRepositoryProducts repositoryProducts = new NetShop.Repository.RepositoryProducts();
            foreach (Entities.Basket bask in basket)
            {
                bask.ProductImage = repositoryProducts.GetMainImage(bask.ProductID);
            }

            BasketViewModel mainModel = new BasketViewModel();
            mainModel.TreeCategories = categories;
            mainModel.BasketItems = basket;
            return View(mainModel);
        }

        public bool AddToBasket(int productID, int count)
        {
            Entities.Basket basket = new Entities.Basket();
            basket.SessionID = System.Web.HttpContext.Current.Session.SessionID;
            basket.ProductCount = count;
            basket.ProductID = productID;

            IRepositoryRealizations repository = new NetShop.Repository.RepositoryRealizations();
            bool isOK = repository.AddToBasket(basket);

            return isOK;
        }

        public bool RemoveItemFromBasket(int productID)
        {
            string sessionID = System.Web.HttpContext.Current.Session.SessionID;
            IRepositoryRealizations repository = new NetShop.Repository.RepositoryRealizations();
            bool isOK = repository.RemoveItemFromBasket(sessionID, productID);

            return isOK;
        }

        public decimal UpdateProductCount(int productID, int productCount)
        {
            string sessionID = System.Web.HttpContext.Current.Session.SessionID;
            IRepositoryRealizations repository = new NetShop.Repository.RepositoryRealizations();
            decimal productPrice = repository.UpdateProductCount(sessionID, productID, productCount);

            return productPrice;
        }

    }
}
