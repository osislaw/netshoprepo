﻿using NetShop.Contracts;
using NetShop.Utils.Const;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class CmsValueTypesController : Controller
    {
        //
        // GET: /CmsValueTypes/

        public ActionResult ValueTypes()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ValueTypes(int jtStartIndex = 0, int jtPageSize = 20, string jtSorting = null, string searchText = null)
        {
            try
            {
                IRepositoryValueTypes repository = new NetShop.Repository.RepositoryValueTypes();
                var valueTypes = repository.Search(jtStartIndex, jtPageSize, string.IsNullOrEmpty(jtSorting) ? "IDvalueType DESC" : jtSorting, searchText);
                var valueTypesAddList = repository.Search(jtStartIndex = 0, jtPageSize = 0, string.IsNullOrEmpty(jtSorting) ? "Type" : jtSorting, searchText: searchText).ValueTypeList.Select(c => new { DisplayText = c.Type, Value = c.IDvalueType });
                var newValueTypesList = new List<object> { new { DisplayText = "", Value = -1 } };
                newValueTypesList.AddRange(valueTypesAddList.ToList().OrderBy(x => x.DisplayText));
                return Json(new { Result = ResultConst.OK, Records = valueTypes.ValueTypeList, TotalRecordCount = valueTypes.TotalCount, Options = newValueTypesList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
