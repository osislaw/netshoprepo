﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class ReturnPolicyController : Controller
    {
        //
        // GET: /ReturnPolicy/

        public ActionResult ReturnPolicy()
        {
            IRepositoryCategories repository = new NetShop.Repository.RepositoryCategories();
            List<TreeCategory> categories = repository.GetTree();

            ShopViewModel model = new ShopViewModel();
            model.TreeCategories = categories;

            return View(model);
        }

    }
}
