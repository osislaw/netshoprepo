﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            IRepositoryCategories repository = new NetShop.Repository.RepositoryCategories();
            List<TreeCategory> categories = repository.GetTree();

            IRepositoryProducts repositoryProducts = new NetShop.Repository.RepositoryProducts();
            Products products = repositoryProducts.Search(0, 9, "Weight");
            foreach(Product product in products.ProductList)
            {
                product.ProductImage = repositoryProducts.GetMainImage(product.IDproduct);
            }

            IRepositoryNews repositoryNews = new NetShop.Repository.RepositoryNews();
            News news = repositoryNews.Search(0, Repository.GlobalSettings.Instance.NewsCount, "CreateDate DESC");

            MainViewModel mainModel = new MainViewModel();
            mainModel.TreeCategories = categories;
            mainModel.Products = products.ProductList;
            mainModel.NewsItem = news.NewsItemList;
            return View(mainModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
