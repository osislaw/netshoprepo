﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class SearchProductController : Controller
    {
        //
        // GET: /SearchProduct/

        public ActionResult SearchProduct(int pageNumber, string searchText)
        {
            // TODO ustawienie globalne waluty
            IRepositoryCategories repository = new NetShop.Repository.RepositoryCategories();
            List<TreeCategory> categories = repository.GetTree();

            IRepositoryProducts repositoryProducts = new NetShop.Repository.RepositoryProducts();
            //TODO ustawienia globalne
            Products products = repositoryProducts.Search(pageNumber*20, 20, "Weight", searchText); 
            foreach (Product product in products.ProductList)
            {
                product.ProductImage = repositoryProducts.GetMainImage(product.IDproduct);
            }

            SearchProductViewModel mainModel = new SearchProductViewModel();
            mainModel.TreeCategories = categories;
            mainModel.ProductList = products.ProductList;
            return View(mainModel);
        }

    }
}
