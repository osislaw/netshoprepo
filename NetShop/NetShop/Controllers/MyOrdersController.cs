﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class MyOrdersController : Controller
    {
        //
        // GET: /MyOrders/

        public ActionResult MyOrders(int pageNumber, int customerID)
        {
            MyOrdersViewModel model = new MyOrdersViewModel();
            model.PageNumber = pageNumber;
            
            IRepositoryCategories repositoryCategories = new NetShop.Repository.RepositoryCategories();
            List<TreeCategory> categories = repositoryCategories.GetTree();
            model.TreeCategories = categories;

            model.CustomerID = customerID;
            int pageSize = 5;
            IRepositoryRealizations repositoryRealizations = new NetShop.Repository.RepositoryRealizations();
            Realizations realizations = repositoryRealizations.GetByCustomer(customerID, pageNumber-1, pageSize);

            model.RealizationList = realizations.RealizationList;
            model.TotalCount = realizations.TotalCount;

            int lastIndex = -1;
            if (realizations.TotalCount % pageSize == 0)
            {
                lastIndex = realizations.TotalCount/pageSize;
            }
            else
            {
                lastIndex = realizations.TotalCount / pageSize + 1;
            }

            model.TotalPageNumber = lastIndex;
                
            return View(model);
        }

    }
}
