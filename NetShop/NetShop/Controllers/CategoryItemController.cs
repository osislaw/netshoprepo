﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class CategoryItemController : Controller
    {
        //
        // GET: /Categories/

        public ActionResult CategoryItem(int categoryID, int pageNumber, string priceSort, string nameSort)
        {
            IRepositoryCategories repository = new NetShop.Repository.RepositoryCategories();
            List<TreeCategory> categories = repository.GetTree();

            string sortBy = string.Empty;
            if (priceSort == "DESC")
            {
                sortBy = "PriceBrutto DESC";
            }
            else
            {
                sortBy = "PriceBrutto";
            }
            if (!(string.IsNullOrEmpty(sortBy)))
            {
                sortBy += ", ";
            }
            if (nameSort == "DESC")
            {
                
                sortBy += "ProductName DESC";
            }
            else
            {
                sortBy += "ProductName";
            }

            if(pageNumber < 1)
            {
                pageNumber = 1;
            }
            IRepositoryProducts repositoryProducts = new NetShop.Repository.RepositoryProducts();
            Products products = repositoryProducts.GetCategoryProducts(categoryID, 20, pageNumber-1, sortBy);
            foreach (Product product in products.ProductList)
            {
                product.ProductImage = repositoryProducts.GetMainImage(product.IDproduct);
            }

            Category category = repository.Get(categoryID);

            CategoryViewModel mainModel = new CategoryViewModel();
            int lastIndex = -1;
            if (products.TotalCount % 20 == 0)
            {
                lastIndex = products.TotalCount/20;
            }
            else
            {
                lastIndex = products.TotalCount / 20 + 1;
            }
            mainModel.PageNumber = pageNumber;
            mainModel.LastIndex = lastIndex;
            mainModel.TreeCategories = categories;
            mainModel.Category = category;
            mainModel.ProductList = products.ProductList;
            return View(mainModel);
        }
    }
}
