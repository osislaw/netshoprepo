﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Utils.Const;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class CmsCategoriesController : Controller
    {
        //
        // GET: /CmsCategories/

        public ActionResult Categories()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Categories(int jtStartIndex = 0, int jtPageSize = 20, string jtSorting = null, string searchText = null)
        {
            try
            {
                IRepositoryCategories repository = new NetShop.Repository.RepositoryCategories();
                var categories = repository.Search(jtStartIndex, jtPageSize, string.IsNullOrEmpty(jtSorting) ? "IDcategory DESC" : jtSorting, searchText);
                var categoriesAddList = repository.Search(jtStartIndex = 0, jtPageSize = 0, string.IsNullOrEmpty(jtSorting) ? "CategoryName" : jtSorting, searchText: searchText).CategoryList.Select(c => new { DisplayText = c.CategoryName, Value = c.IDcategory });
                var newCategoryList = new List<object> { new { DisplayText = "", Value = -1 } };
                newCategoryList.AddRange(categoriesAddList.ToList().OrderBy(x => x.DisplayText));
                return Json(new { Result = ResultConst.OK, Records = categories.CategoryList, TotalRecordCount = categories.TotalCount, Options = newCategoryList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CreateCategory(Category category)
        {
            try
            {
                IRepositoryCategories repository = new NetShop.Repository.RepositoryCategories();
                int id = repository.Add(category);
                if (id > 0)
                {
                    category = repository.Get(id);
                    return Json(new { Result = ResultConst.OK, Record = category }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = "Błąd dodania kategoria" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteCategory(int IDcategory)
        {
            try
            {
                IRepositoryCategories repository = new NetShop.Repository.RepositoryCategories();
                bool isOk = repository.Delete(IDcategory);
                if (isOk)
                {
                    return Json(new { Result = ResultConst.OK }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = "Błąd usunięcia kategorii" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateCategory(Category category)
        {
            try
            {
                IRepositoryCategories repository = new NetShop.Repository.RepositoryCategories();
                bool isOK = repository.Update(category);
                if (isOK)
                {
                    category = repository.Get(category.IDcategory);
                    return Json(new { Result = ResultConst.OK, Record = category }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = "Błąd aktualizacji kategorii" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetChildrenCategory(int parentID)
        {
            try
            {
                IRepositoryCategories repository = new NetShop.Repository.RepositoryCategories();
                List<Category> categories = repository.GetChildrenCategory(parentID);
                return Json(new { Result = ResultConst.OK, Records = categories }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SearchCategories(int jtStartIndex = 0, int jtPageSize = 20, string jtSorting = null, string searchText = null)
        {
            try
            {
                IRepositoryCategories repository = new NetShop.Repository.RepositoryCategories();
                var categories = repository.SearchCategories(jtStartIndex, jtPageSize, string.IsNullOrEmpty(jtSorting) ? "IDcategory DESC" : jtSorting, searchText);
                var categoriesAddList = repository.SearchCategories(jtStartIndex = 0, jtPageSize = 0, string.IsNullOrEmpty(jtSorting) ? "CategoryName" : jtSorting, searchText: searchText).CategoryList.Select(c => new { DisplayText = c.CategoryName, Value = c.IDcategory });
                var newCategoryList = new List<object> { new { DisplayText = "", Value = -1 } };
                newCategoryList.AddRange(categoriesAddList.ToList().OrderBy(x => x.DisplayText));
                return Json(new { Result = ResultConst.OK, Records = categories.CategoryList, TotalRecordCount = categories.TotalCount, Options = newCategoryList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
