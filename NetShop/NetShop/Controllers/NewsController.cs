﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class NewsController : Controller
    {
        //
        // GET: /News/

        public ActionResult NewsItem(int newsID)
        {
            IRepositoryCategories repository = new NetShop.Repository.RepositoryCategories();
            List<TreeCategory> categories = repository.GetTree();

            IRepositoryProducts repositoryProducts = new NetShop.Repository.RepositoryProducts();
            Products products = repositoryProducts.Search(0, 9, "Weight");
            foreach (Product product in products.ProductList)
            {
                product.ProductImage = repositoryProducts.GetMainImage(product.IDproduct);
            }

            IRepositoryNews repositoryNews = new NetShop.Repository.RepositoryNews();
            NewsItem news = repositoryNews.Get(newsID);

            MainViewModel mainModel = new MainViewModel();
            mainModel.TreeCategories = categories;
            mainModel.Products = products.ProductList;
            mainModel.NewsItem = new List<NewsItem> { news };
            return View(mainModel);
        }

    }
}
