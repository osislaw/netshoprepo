﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Utils.Const;
using NetShop.Utils.Errors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Controllers
{
    public class CmsRealizationsController : Controller
    {
        //
        // GET: /CmsRealizations/

        public ActionResult Realizations()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Realizations(int jtStartIndex = 0, int jtPageSize = 20, string jtSorting = null, string searchText = null)
        {
            try
            {
                IRepositoryRealizations repository = new NetShop.Repository.RepositoryRealizations();
                var realizations = repository.Search(jtStartIndex, jtPageSize, string.IsNullOrEmpty(jtSorting) ? "IDrealization" : jtSorting, searchText);
                var statusList = repository.GetStatusList().Select(c => new { DisplayText = c.StatusName, Value = c.RealizationStatusID.ToString() });
                return Json(new { Result = ResultConst.OK, Records = realizations.RealizationList, TotalRecordCount = realizations.TotalCount, Options = statusList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateRealizations(Realization realization)
        {
            try
            {
                if (realization.PaymentID < 1 || realization.ShipmentID < 1)
                {
                    return Json(new { Result = ResultConst.ERROR, Message = RealizationErrors.ERRORPAYMENTANDSHIPMENT }, JsonRequestBehavior.AllowGet);
                }
                IRepositoryRealizations repository = new NetShop.Repository.RepositoryRealizations();
                bool isOK = repository.Update(realization);
                if (isOK)
                {
                    Realization real = repository.Get(realization.IDrealization);
                    return Json(new { Result = ResultConst.OK, Record = real }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = RealizationErrors.ERRORINUPDATE }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteRealizations(int IDrealization)
        {
            try
            {
                IRepositoryRealizations repository = new NetShop.Repository.RepositoryRealizations();
                var isOK = repository.Delete(IDrealization);
                if (isOK)
                {
                    return Json(new { Result = ResultConst.OK }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = RealizationErrors.ERRORINDELETE }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult RealizationItems(int realizationID)
        {
            try
            {
                IRepositoryRealizations repository = new NetShop.Repository.RepositoryRealizations();
                var realizationItems = repository.GetRealizationItems(realizationID);
                return Json(new { Result = ResultConst.OK, Records = realizationItems.RealizationItemList, TotalRecordCount = realizationItems.TotalCount }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult AddItem(RealizationItem realizationItem)
        {
            try
            {
                IRepositoryRealizations repository = new NetShop.Repository.RepositoryRealizations();

                List<RealizationItem> realizationItems = new List<RealizationItem>();
                for (int i = 0; i < realizationItem.ProductCount; i++)
                {
                    realizationItems.Add(repository.AddItemToRealization(realizationItem));
                }
                return Json(new { Result = ResultConst.OK, Record = realizationItems.First() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateItem(RealizationItem realizationItem)
        {
            try
            {
                IRepositoryRealizations repository = new NetShop.Repository.RepositoryRealizations();
                realizationItem = repository.UpdateItemInRealization(realizationItem);
                if (realizationItem != null)
                {
                    return Json(new { Result = ResultConst.OK, Record = realizationItem }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = RealizationErrors.ERRORINUPDATEITEM }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteItem(int IDrealizationItem)
        {
            try
            {
                IRepositoryRealizations repository = new NetShop.Repository.RepositoryRealizations();
                bool isOK = repository.DeleteItemFromRealization(IDrealizationItem);
                if (isOK)
                {
                    return Json(new { Result = ResultConst.OK }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = RealizationErrors.ERRORINDELETEITEM }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
