﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Services;
using NetShop.Utils;
using NetShop.Utils.Const;
using NetShop.Utils.Errors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace NetShop.Controllers
{
    public class CmsCustomersController : Controller
    {
        //
        // GET: /CmsCustomers/

        public ActionResult Customers()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Customers(int jtStartIndex = 0, int jtPageSize = 20, string jtSorting = null, string searchText = null)
        {
            try
            {
                IRepositoryCustomers repository = new NetShop.Repository.RepositoryCustomers();
                var customers = repository.Search(jtStartIndex, jtPageSize, string.IsNullOrEmpty(jtSorting) ? "IDcustomer DESC" : jtSorting, searchText);
                var customerList = repository.Search(0,0).CustomerList.Select(c => new { DisplayText = c.FirstName + " " + c.Surname + " " + c.CompanyName, Value = c.IDcustomer.ToString() });
                return Json(new { Result = ResultConst.OK, Records = customers.CustomerList, TotalRecordCount = customers.TotalCount, Options = customerList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CreateCustomer(Customer customer)
        {
            try
            {
                IRepositoryCustomers repository = new NetShop.Repository.RepositoryCustomers();
                if (!WebSecurity.Initialized)
                {
                    WebSecurity.InitializeDatabaseConnection("DefaultConnection", "UserProfile", "UserId", "UserName", autoCreateTables: false);
                }
                if (!WebSecurity.UserExists(customer.UserName))
                {
                    int id = CustomersServices.CreateCustomer(customer);
                    if (id > 0)
                    {
                        customer = repository.Get(id);
                        return Json(new { Result = ResultConst.OK, Record = customer }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { Result = ResultConst.ERROR, Message = CustomersErrors.CLIENT_ADD_ERROR }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = CustomersErrors.USERNAME_EXISTS }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult UpdateCustomer(Customer customer)
        {
            try
            {
                IRepositoryCustomers repository = new NetShop.Repository.RepositoryCustomers();
                IRepositoryUserProfiles userProfilesRepo = new NetShop.Repository.RepositoryUserProfiles();
                bool isOK = repository.Update(customer);
                if (isOK)
                {
                    //userProfilesRepo.UpdateUserName(customer.UserName, customer.UserProfileID);
                    if (!String.IsNullOrEmpty(customer.Password))
                    {
                        CustomersServices.ChangeCustomerPassword(customer.UserName, customer.Password);
                    }
                    customer = repository.Get(customer.IDcustomer);
                    return Json(new { Result = ResultConst.OK, Record = customer }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = CustomersErrors.CLIENT_UPDATE_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteCustomer(int IDcustomer)
        {
            try
            {
                IRepositoryCustomers repository = new NetShop.Repository.RepositoryCustomers();

                bool isOK = repository.Delete(IDcustomer);
                if (isOK)
                {
                    Customer cust = repository.Get(IDcustomer);
                    if (Membership.DeleteUser(cust.UserName))
                    {
                        return Json(new { Result = ResultConst.OK }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        cust = repository.Get(IDcustomer);
                        cust.IsDeleted = false;
                        repository.Update(cust);
                        return Json(new { Result = ResultConst.ERROR, Message = CustomersErrors.CLIENT_DELETE_ERROR }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { Result = ResultConst.ERROR, Message = CustomersErrors.CLIENT_DELETE_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Result = ResultConst.ERROR, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
