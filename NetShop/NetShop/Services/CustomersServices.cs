﻿using NetShop.Contracts;
using NetShop.Contracts.Class;
using NetShop.Models;
using NetShop.Utils.Const;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using WebMatrix.WebData;
using AutoMapper;

namespace NetShop.Services
{
    public static class CustomersServices
    {
        public static int CreateCustomerUser(string userName, string password)
        {
            WebSecurity.CreateUserAndAccount(userName, password);
            Roles.AddUserToRole(userName, RolesConst.Customer);
            return WebSecurity.GetUserId(userName);
        }
        public static int CreateCustomerUserWithToken(string userName, string password)
        {
            string token = WebSecurity.CreateUserAndAccount(userName, password, null, true);
            Roles.AddUserToRole(userName, RolesConst.Customer);
            return WebSecurity.GetUserId(userName);
        }

        public static int CreateCustomer(CustomerViewModel customerViewModel)
        {
            Mapper.CreateMap<CustomerViewModel, Customer>();
            Mapper.CreateMap<Customer, CustomerViewModel>();

            Customer customer = Mapper.Map<Customer>(customerViewModel);

            //w razie gdybysmy chcieli wysylac tokeny dajemy na true - dodatkowo dodac obsluge maila + formatka z obsluga tokena
            return CreateCustomer(customer, false);
        }

        public static int CreateCustomer(Customer customer, bool token = false)
        {
            if (ValidateCustomer(customer))
            {
                IRepositoryCustomers repository = new NetShop.Repository.RepositoryCustomers();
                if (token)
                {
                    customer.UserProfileID = CustomersServices.CreateCustomerUserWithToken(customer.UserName, customer.Password);
                }
                else
                {
                    customer.UserProfileID = CustomersServices.CreateCustomerUser(customer.UserName, customer.Password);
                }
                return repository.Add(customer);
            }
            return -1;
        }

        public static void ChangeCustomerPassword(string userName, string newPassword)
        {
            WebSecurity.ResetPassword(WebSecurity.GeneratePasswordResetToken(userName), newPassword);
        }

        private static bool ValidateCustomer(Customer customer)
        {
            if(customer != null && !String.IsNullOrEmpty(customer.FirstName) &&
               !String.IsNullOrEmpty(customer.Surname) && !String.IsNullOrEmpty(customer.City) &&
               !String.IsNullOrEmpty(customer.Street) && !String.IsNullOrEmpty(customer.ZipCode) &&
               !String.IsNullOrEmpty(customer.UserName) && !String.IsNullOrEmpty(customer.Password))
            {
                return true;
            }
            return false;
        }
    }
}
