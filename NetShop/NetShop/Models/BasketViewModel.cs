﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetShop.Models
{
    public class BasketViewModel
    {
        private List<Basket> basketItems;
        private List<TreeCategory> treeCategories;

        public List<TreeCategory> TreeCategories
        {
            get { return treeCategories; }
            set { treeCategories = value; }
        }
        public List<Basket> BasketItems
        {
            get { return basketItems; }
            set { basketItems = value; }
        }
    }
}