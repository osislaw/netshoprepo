﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetShop.Models
{
    public class ShopViewModel
    {
        private List<TreeCategory> treeCategories;
        private List<Product> products;

        public List<Product> Products
        {
            get { return products; }
            set { products = value; }
        }

        public List<TreeCategory> TreeCategories
        {
            get { return treeCategories; }
            set { treeCategories = value; }
        }
    }
}