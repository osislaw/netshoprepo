﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace NetShop.Models
{
    public class LoginViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        public string Email { get; set; }

        public bool RememberMe { get; set; }

        public List<TreeCategory> TreeCategories { get; set; }
    }
}
