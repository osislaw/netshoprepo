﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace NetShop.Models
{
    public class SettingsViewModel
    {
        private bool firstRun;
        private int newsCount;
        private string currency;
        private string shopName;
        private string contact;
        private string regulations;
        private string cookies;
        private string returnPolicy;

        public bool FirstRun
        {
            get
            {
                return firstRun;
            }

            set
            {
                firstRun = value;
            }
        }

        [Required]
        [Display(Name = "Ilość newsów na stronie głównej")]
        [RegularExpression("([0-9][0-9]*)", ErrorMessage = "Proszę wpisać liczbę!")]
        public int NewsCount
        {
            get
            {
                return newsCount;
            }

            set
            {
                newsCount = value;
            }
        }

        public string Currency
        {
            get
            {
                return currency;
            }

            set
            {
                currency = value;
            }
        }

        public string ShopName
        {
            get
            {
                return shopName;
            }

            set
            {
                shopName = value;
            }
        }

        public string Contact
        {
            get
            {
                return contact;
            }

            set
            {
                contact = value;
            }
        }

        public string Regulations
        {
            get
            {
                return regulations;
            }

            set
            {
                regulations = value;
            }
        }

        public string Cookies
        {
            get
            {
                return cookies;
            }

            set
            {
                cookies = value;
            }
        }

        public string ReturnPolicy
        {
            get
            {
                return returnPolicy;
            }

            set
            {
                returnPolicy = value;
            }
        }

    }
}
