﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetShop.Models
{
    public class DetailsViewModel
    {
        private List<TreeCategory> treeCategories;
        private Customer customer;
        private string comment;
        private decimal totalPriceBrutto;
        private int paymentID;
        private IEnumerable<PaymentType> paymentTypes;
        private int shipmentID;
        private IEnumerable<ShipmentType> shipmentTypes;

        public IEnumerable<PaymentType> PaymentTypes
        {
            get { return paymentTypes; }
            set { paymentTypes = value; }
        }

        public IEnumerable<ShipmentType> ShipmentTypes
        {
            get { return shipmentTypes; }
            set { shipmentTypes = value; }
        }

        public int ShipmentID
        {
            get { return shipmentID; }
            set { shipmentID = value; }
        }

        public int PaymentID
        {
            get { return paymentID; }
            set { paymentID = value; }
        }

        public decimal TotalPriceNetto
        {
            get { return TotalPriceBrutto / 1.23m; }
        }

        public decimal TotalVat
        {
            get { return TotalPriceBrutto - (TotalPriceBrutto / 1.23m); }
        }

        public decimal TotalPriceBrutto
        {
            get { return totalPriceBrutto; }
            set { totalPriceBrutto = value; }
        }

        public string Comment
        {
            get { return comment; }
            set { comment = value; }
        }

        public Customer Customer
        {
            get { return customer; }
            set { customer = value; }
        }
        
        public List<TreeCategory> TreeCategories
        {
            get { return treeCategories; }
            set { treeCategories = value; }
        }
    }
}