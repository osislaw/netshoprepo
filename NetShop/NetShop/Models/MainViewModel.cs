﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetShop.Models
{
    public class MainViewModel : ShopViewModel
    {
        private List<NewsItem> newsItem;

        public List<NewsItem> NewsItem
        {
            get { return newsItem; }
            set { newsItem = value; }
        }
    }
}