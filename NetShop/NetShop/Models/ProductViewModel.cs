﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetShop.Models
{
    public class ProductViewModel : ShopViewModel
    {
        private Product productItem;
        private List<Image> imageList;

        public List<Image> ImageList
        {
            get { return imageList; }
            set { imageList = value; }
        }

        public Product ProductItem
        {
            get { return productItem; }
            set { productItem = value; }
        }
    }
}