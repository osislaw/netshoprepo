﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetShop.Models
{
    public class ResultViewModel
    {
        private int realizationID;
        private decimal? totalAmountBrutto;
        private string payment;
        private string shipment;

        public string Shipment
        {
            get { return shipment; }
            set { shipment = value; }
        }

        public string Payment
        {
            get { return payment; }
            set { payment = value; }
        }

        public decimal? TotalAmountBrutto
        {
            get { return totalAmountBrutto; }
            set { totalAmountBrutto = value; }
        }

        public int RealizationID
        {
            get { return realizationID; }
            set { realizationID = value; }
        }
    }
}