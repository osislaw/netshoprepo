﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace NetShop.Models
{
    public class CustomerViewModel
    {
        private int iDcustomer;
        private string firstName;
        private string surname;
        private string city;
        private string street;
        private string zipCode;
        private string companyNIP;
        private string companyCity;
        private string companyStreet;
        private string companyZipCode;
        private int userProfileID;
        private string email;
        private string phone;
        private bool isDeleted;
        private string companyName;

        //login/haslo do logowania
        private string userName;
        private string password;
        private string confirmPassword;

        public string CompanyName
        {
            get { return companyName; }
            set { companyName = value; }
        }

        public bool IsDeleted
        {
            get { return isDeleted; }
            set { isDeleted = value; }
        }

        [Required]
        [Display(Name = "Numer telefonu")]
        [RegularExpression("([0-9][0-9]*)", ErrorMessage = "Numer telefonu powinien zawierać same cyfry!")]
        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

        [Required]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string CompanyZipCode
        {
            get { return companyZipCode; }
            set { companyZipCode = value; }
        }

        public string CompanyStreet
        {
            get { return companyStreet; }
            set { companyStreet = value; }
        }

        public string CompanyCity
        {
            get { return companyCity; }
            set { companyCity = value; }
        }

        [RegularExpression("([0-9][0-9]*)", ErrorMessage = "NIP powinien zawierać same cyfry!")]
        public string CompanyNIP
        {
            get { return companyNIP; }
            set { companyNIP = value; }
        }

        [Required]
        [Display(Name = "Kod pocztowy")]
        public string ZipCode
        {
            get { return zipCode; }
            set { zipCode = value; }
        }

        [Required]
        [Display(Name = "Ulica")]
        public string Street
        {
            get { return street; }
            set { street = value; }
        }

        [Required]
        [Display(Name = "Miasto")]
        public string City
        {
            get { return city; }
            set { city = value; }
        }

        [Required]
        [Display(Name = "Nazwisko")]
        public string Surname
        {
            get { return surname; }
            set { surname = value; }
        }

        [Required]
        [Display(Name = "Imię")]
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        public int IDcustomer
        {
            get { return iDcustomer; }
            set { iDcustomer = value; }
        }

        public int UserProfileID
        {
            get
            {
                return userProfileID;
            }

            set
            {
                userProfileID = value;
            }
        }

        [Required]
        [Display(Name = "Login")]
        public string UserName
        {
            get
            {
                return userName;
            }

            set
            {
                userName = value;
            }
        }

        [Required]
        [Display(Name = "Hasło")]
        [DataType(DataType.Password)]
        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        [Required]
        [Display(Name = "Powtórz hasło")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Hasła nie pasują do siebie.")]
        public string ConfirmPassword
        {
            get
            {
                return confirmPassword;
            }

            set
            {
                confirmPassword = value;
            }
        }
    }
}
