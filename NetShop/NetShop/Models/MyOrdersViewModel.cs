﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetShop.Models
{
    public class MyOrdersViewModel : ShopViewModel
    {
        private int pageNumber;
        private int totalCount;
        private int totalPageNumber;
        private List<Realization> realizationList;
        private int customerID;

        public int CustomerID
        {
            get { return customerID; }
            set { customerID = value; }
        }

        public List<Realization> RealizationList
        {
            get { return realizationList; }
            set { realizationList = value; }
        }

        public int TotalPageNumber
        {
            get { return totalPageNumber; }
            set { totalPageNumber = value; }
        }

        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; }
        }

        public int PageNumber
        {
            get { return pageNumber; }
            set { pageNumber = value; }
        }
    }
}