﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetShop.Models
{
    public class MyAccountViewModel : ShopViewModel
    {
        private Customer customer;
        private string result;
        private bool error;

        public bool Error
        {
            get { return error; }
            set { error = value; }
        }

        public string Result
        {
            get { return result; }
            set { result = value; }
        }

        public Customer Customer
        {
            get { return customer; }
            set { customer = value; }
        }
    }
}