﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetShop.Models
{
    public class OrderViewModel : ShopViewModel
    {
        private Realization realization;
        private List<Order> realizationItems;
        private int customerID;

        public int CustomerID
        {
            get { return customerID; }
            set { customerID = value; }
        }

        public List<Order> RealizationItems
        {
            get { return realizationItems; }
            set { realizationItems = value; }
        }

        public Realization Realization
        {
            get { return realization; }
            set { realization = value; }
        }
    }
}