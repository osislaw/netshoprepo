﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetShop.Models
{
    public class CategoryViewModel : ShopViewModel
    {
        private Category category;
        private List<Product> productList;
        private string searchText;
        private int pageNumber;
        private int lastIndex;

        public int LastIndex
        {
            get { return lastIndex; }
            set { lastIndex = value; }
        }

        public int PageNumber
        {
            get { return pageNumber; }
            set { pageNumber = value; }
        }

        public string SearchText
        {
            get { return searchText; }
            set { searchText = value; }
        }

        public List<Product> ProductList
        {
            get { return productList; }
            set { productList = value; }
        }

        public Category Category
        {
            get { return category; }
            set { category = value; }
        }
    }
}