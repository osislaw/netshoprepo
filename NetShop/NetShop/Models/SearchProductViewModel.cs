﻿using NetShop.Contracts.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetShop.Models
{
    public class SearchProductViewModel : ShopViewModel
    {
        private List<Product> productList;
        private string searchText;

        public string SearchText
        {
            get { return searchText; }
            set { searchText = value; }
        }

        public List<Product> ProductList
        {
            get { return productList; }
            set { productList = value; }
        }
    }
}